<?php

	include_once('../transaction.php');
	include_once('../job/job.php');

	if(empty($_POST['buildingId']))
	{
		echo "1|buildingId is empty";
		die;
	}

	$buildingId = $_POST['buildingId'];

	// Removing from grid
	$db = CDB::getDb();
	$req = $db->prepare("DELETE FROM user_building WHERE id=:buildingId");
	$req->bindParam(':buildingId', $buildingId);
	$req->execute();

	if(!$req)
	{
		echo "2|Unable to delete from grid";
		die;
	}

	// Getting job, if there is one
	$job = CJob::getActiveBuildingJob($buildingId);
	
	if($job != null)
	{
		// Delete first job only
		$req = $db->prepare("DELETE FROM user_job WHERE id=:jobId");
		$req->bindParam(':jobId', $job[0]['job_active_id']);
		$req->execute();

	}

	echo '0|' . $buildingId;
?>