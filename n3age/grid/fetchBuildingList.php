<?php 

	include_once('../CDB.php');

	// Select Buildings
	$db = CDB::getDb();
	$req = $db->prepare("SELECT * FROM building");
	$req->execute();

	$builds = $req->fetchAll(PDO::FETCH_ASSOC);
	if(empty($builds))
	{
		echo "1 - Unable to fetch buildings";
		die;
	}

	echo json_encode($builds);

?>