<?php

	include_once('../transaction.php');

	// Pulling grid from DB
//	$db = CDB::getDb();
//	$req = $db->prepare("SELECT * FROM grid where user_id=:userId");
//	$req->bindParam(':userId', $userId);
//	$req->execute();
//	$grid = $req->fetch(PDO::FETCH_ASSOC);

	$req = $db->prepare("SELECT user_building.id,user_building.data_id,user_building.pos_x,user_building.pos_y,building.name,building.size_x,building.size_y,building.model_id FROM user_building LEFT JOIN building ON data_id=building.id where user_id=:userId");
	$req->bindParam(':userId', $userId);
	$req->execute();
	$building = $req->fetchAll(PDO::FETCH_ASSOC);

	echo '0|' . json_encode($building);
?>