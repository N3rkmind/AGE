<?php

	include_once('../transaction.php');
	include_once('../job/job.php');

	if(empty($_POST['buildingId']))
	{
		echo "1|buildingId is empty";
		die;
	}

	if(empty($_POST['pos']))
	{
		echo "2|pos is empty";
		die;
	}


	$buildingId = $_POST['buildingId'];
	$posData = explode('|',$_POST['pos']);
	$posX = $posData[0];
	$posY = $posData[1];

	$job = CJob::fetchBuildJob($buildingId);
	if(empty($job))
	{
		echo "3|unable to find job";
		die;
	}

	if($posX == -1)
	{
		echo "4|out of the grid";
		die;
	}

	// Can we place that here?
	//$$
	if(false)
	{
		echo "5|unable to place object here";
		die;
	}

	// Do we have enough ressources
	//$$
	if(false)
	{
		echo "6|Don't have enough ressources";
		die;
	}

	// Adding to user_building
	$db = CDB::getDb();
	$req = $db->prepare("INSERT INTO user_building (data_id,user_id,pos_x,pos_y) VALUES (:dataId,:userId,:posX,:posY)");
	$req->bindParam(':dataId', $buildingId);
	$req->bindParam(':userId', $userId);
	$req->bindParam(':posX', $posX);
	$req->bindParam(':posY', $posY);
	$req->execute();
	$activeId = CDB::lastInsertedId();

	// Starting a build job
	CJob::startBuildJob($job,$activeId,$userId);
	$jobActiveId = CDB::lastInsertedId();
	// OPCODE | BUILDING ACTIVE ID | JOB ACTIVE ID |
	echo '0|' . $activeId . '|' . $jobActiveId;
?>