<?php

	include_once "../transaction.php";
	include_once "job.php";

	if(empty($_POST['activeId']))
	{
		echo "1|activeId is empty";
		die;
	}

	if(empty($_POST['dataId']))
	{
		echo "2|dataId is empty";
		die;
	}

	$dataId = $_POST['dataId'];
	$activeId = $_POST['activeId'];

	$job = CJob::fetchUpgradeJob($dataId);
	if(empty($job))
	{
		echo "1|Upgrade job not found";
		die;
	}

	$activeJob = CJob::getActiveBuildingJob($activeId);
	CJob::stopJob($activeJob['job_active_id']);

	CJob::startUpgradeJob($job,$activeId,$userId);
	$lastId = CDB::lastInsertedId();

	echo "0" . '|'  . $dataId . '|' . $lastId . '|' .$activeId;
?>