<?php

	include_once "../transaction.php";
	include_once "../resource/resource.php";
	include_once "job.php";

	if(empty($_POST['jobId']))
	{
		echo "1|jobId is empty";
		die;
	}

	$jobId = $_POST['jobId'];

	// Select Job
	$db = CDB::getDb();
	$req = $db->prepare("SELECT caller_data_id,job.reward_id,user_job.caller_id AS caller_id FROM job INNER JOIN user_job ON user_job.job_id = job.id WHERE user_job.id=:jobId");
	$req->bindParam(':jobId', $jobId);
	$req->execute();
	$job = $req->fetch(PDO::FETCH_ASSOC);

	// Update Resource
	CResource::GiveResource($job['reward_id'],$userId);
	
	// Update Job
	$req = $db->prepare("UPDATE user_job SET completed=1 WHERE id=:jobId");
	$req->bindParam(':jobId', $jobId);
	$req->execute();

	// Start Automated Job if any
	$gather = CJob::fetchGatherJob($job['caller_data_id']);
	if($gather != null)
	{
		CJob::startGatherJob($gather['job_id'],$job['caller_id'],$userId);
	}

	$gatherJobId = CDB::lastInsertedId();

	echo '0|' . $gatherJobId . '|' . $job['caller_id'];
?>