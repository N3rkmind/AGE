<?php 

	include_once("../CDB.php");
	include_once("../cost/cost.php");

	class Job
	{
		public static $name;
		public static $callerTarget;
		public static $callerId;

		// Cost
		public static $resources;
		public static $resourceCosts;
		public static $duration;

		// Reward
		public static $rewardTarget;
		public static $rewardId;
		public static $rewardParam;
	}

	class CJob
	{
		public static function getActiveBuildingJob($buildingActiveId)
		{
			$db = CDB::getDb();
			$req = $db->prepare("SELECT *,user_job.id AS job_active_id FROM user_job INNER JOIN job ON user_job.job_id = job.id WHERE caller_id=:buildingId AND job.caller_table='building' AND completed=0");
			$req->bindParam(':buildingId', $buildingActiveId);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public static function fetchBuildJob($buildingId)
		{
			$db = CDB::getDb();
			$req = $db->prepare("SELECT * FROM job INNER JOIN cost ON cost.job_id = job.id WHERE job.caller_data_id=:buildingId AND job.type = 'build'");
			$req->bindParam(':buildingId', $buildingId);
			$req->execute();

			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public static function fetchGatherJob($buildingDataId)
		{
			$db = CDB::getDb();
			$req = $db->prepare("SELECT *,job.id AS job_id FROM job INNER JOIN cost WHERE job.caller_data_id = :buildingId AND job.type = 'gather'");
			$req->bindParam(':buildingId', $buildingDataId);
			$req->execute();
			$r = $req->fetch(PDO::FETCH_ASSOC);
			return $r;
		}

		public static function fetchUpgradeJob($buildingDataId)
		{
			$db = CDB::getDb();
			$req = $db->prepare("SELECT *,job.id AS job_id FROM job INNER JOIN cost WHERE job.caller_data_id = :buildingId AND job.type = 'upgrade'");
			$req->bindParam(':buildingId', $buildingDataId);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public static function fetchJobByName($name)
		{
			$db = CDB::getDb();
			$req = $db->prepare("SELECT * FROM job INNER JOIN cost WHERE job.cost_id = cost.id AND job.name = ':jobName'");
			$req->bindParam(':jobName', $name);
			$req->execute();
			return $req->fetch(PDO::FETCH_ASSOC);
		}

		public static function startBuildJob($job,$callerId,$userId)
		{
			$jobId = $job['job_id'];
			$startTime = time();
			$db = CDB::getDb();

			// Check for the cost
			CCost::ChargeCost($jobId,$userId);

			// Add job
			$req = $db->prepare("INSERT INTO user_job (user_id,job_id,caller_id,target_id,start_time) VALUES (:userId,:jobId,:callerId,:targetId,:startTime)");
			$req->bindParam(':userId', $userId);
			$req->bindParam(':jobId', $jobId);
			$req->bindParam(':callerId', $callerId);
			$req->bindParam(':targetId', $callerId);
			$req->bindParam(':startTime', $startTime);
			$req->execute();
		}

		public static function startGatherJob($jobId,$callerId,$userId)
		{
			$startTime = time();
			$db = CDB::getDb();

			$req = $db->prepare("SELECT * FROM user_job WHERE caller_id=:callerId AND completed=0");
			$req->bindParam(':callerId', $callerId);
			$req->execute();
			$r = $req->fetch(PDO::FETCH_ASSOC);
			if($r != null)
			{
				echo "startGatherJob::unable to fetch job";
				die;
			}

			$req = $db->prepare("INSERT INTO user_job (user_id,job_id,caller_id,target_id,start_time) VALUES (:userId,:jobId,:callerId,:targetId,:startTime)");
			$req->bindParam(':userId', $userId);
			$req->bindParam(':jobId',$jobId);
			$req->bindParam(':callerId', $callerId);
			$req->bindParam(':targetId', $callerId);
			$req->bindParam(':startTime', $startTime);
			$req->execute();
		}

		public static function startUpgradeJob($job,$callerId,$userId)
		{
			$startTime = time();
			$db = CDB::getDb();
			$jobId = $job['job_id'];

			$req = $db->prepare("INSERT INTO user_job (user_id,job_id,caller_id,target_id,start_time) VALUES (:userId,:jobId,:callerId,:targetId,:startTime)");
			$req->bindParam(':userId', $userId);
			$req->bindParam(':jobId', $jobId);
			$req->bindParam(':callerId', $callerId);
			$req->bindParam(':targetId', $callerId);
			$req->bindParam(':startTime', $startTime);
			$req->execute();
		}

		public static function stopJob($jobId)
		{
			$db = CDB::getDb();
			$req = $db->prepare("DELETE FROM user_job WHERE id=:jobId");
			$req->bindParam(':jobId', $jobId);
			$req->execute();
		}
	}

?>