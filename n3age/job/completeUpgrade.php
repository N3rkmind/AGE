<?php

	include_once "../transaction.php";
	include_once "job.php";

	if(empty($_POST['jobId']))
	{
		echo "1|jobId is empty";
		die;
	}

	$jobId = $_POST['jobId'];

	$db = CDB::getDb();

	// Fetch Job & New Data Id
	$req = $db->prepare("SELECT reward.target_id AS new_building,user_job.caller_id AS caller_id FROM job INNER JOIN user_job ON job.id = user_job.job_id INNER JOIN reward ON job.reward_id=reward.id WHERE user_job.id=:jobId");
	$req->bindParam(':jobId', $jobId);
	$req->execute();
	$data = $req->fetch(PDO::FETCH_ASSOC);

	// Update Job
	$req = $db->prepare("UPDATE user_job SET completed=1 WHERE id=:jobId");
	$req->bindParam(':jobId', $jobId);
	$req->execute();

	// Update Building
	$req = $db->prepare("UPDATE user_building SET data_id=:newBuilding WHERE id=:callerId");
	$req->bindParam(':newBuilding', $data['new_building']);
	$req->bindParam(':callerId', $data['caller_id']);
	$req->execute();

	// Start Automated Job if any
	$gather = CJob::fetchGatherJob($data['new_building']);
	if($gather != null)
	{
		CJob::startGatherJob($gather['job_id'],$data['caller_id'],$userId);
	}

	$gatherJobId = CDB::lastInsertedId();

	echo '0|' . $gatherJobId . '|' . $data['new_building'];
?>