<?php

	include "../transaction.php";

	// Fetching the resources
	$db = CDB::getDb();
	$req = $db->prepare("SELECT user_job.id AS active_id, job.id data_id, job.name, job.type,job.caller_data_id, job.target_table, job.reward_id, job.caller_table, user_job.caller_id, user_job.target_id, user_job.start_time, user_job.completed FROM user_job INNER JOIN job ON job.id = user_job.job_id WHERE user_job.user_id=:userId AND completed=0");
	$req->bindParam(':userId', $userId);
	$req->execute();
	$res = $req->fetchAll(PDO::FETCH_ASSOC);

	if(!$req)
	{
		echo "1|Unable to select user jobs";
	}

	$responseString = "";
	for ($i=0; $i < count($res); $i++)
	{ 
		$responseString .= $res[$i]['active_id'] . '|' . $res[$i]['data_id'] . '|' . $res[$i]['name'] . '|' . $res[$i]['type'] . '|' . $res[$i]['caller_data_id'] . '|' . $res[$i]['caller_table'] . '|' . $res[$i]['caller_id'] . '|' . $res[$i]['target_table'] . '|' . $res[$i]['target_id'] . '|' . $res[$i]['reward_id'] . '|' . $res[$i]['start_time'] . '|' . $res[$i]['completed'] . '%';
	}
	$responseString = substr($responseString, 0, -1);
	
	echo "0%" . $responseString;
?>