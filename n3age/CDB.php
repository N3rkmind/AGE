<?php 


header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time, Auth");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Origin: *");

class CDB
{
	// Database identifier
	private static $host = "localhost";
	//private static $host = "107.180.27.240";
	private static $dbName = "n3age";
	private static $dbUser = "ageadmin";
	private static $dbPass = "e?^BmWu+[+#Vd8B";
	private static $instance = null;

	private function __construct() {
		$getDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	/**
	* Returns the unique instance of PDO object
	* @return PDO
	*/
	public static function getDb()
	{
		if(is_null(self::$instance))
		{
			self::$instance = new PDO("mysql:host=" .self::$host. ";dbname=" .self::$dbName, self::$dbUser, self::$dbPass);
			self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		return self::$instance;
	}
	
	// Execute statement, use in that order
	public static function prepareSQL($query)
	{
		return self::getDb()->prepare($query);
	}
	
	/**
	 * Executes an SQL command and returns if succeded
	 * @return boolean
	 */
	public static function execute($SQL)
	{
		try
		{
			self::getDb()->query($SQL);
			return true;
		}
		catch(Exception $e)
		{
			echo "SQL Execution Error<br />";
			echo "Request: $SQL";
			echo $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Returns all the data fetched from the SQL request
	 * @return array(array(mixed))
	 */
	public static function fetchAll($SQL, $assoc = true)
	{
		$return = null;
		try
		{
			if($assoc)
				$return = self::getDb()->query($SQL)->fetchAll(PDO::FETCH_ASSOC);
			else
				$return = self::getDb()->query($SQL)->fetchAll();
		}
		catch(Exception $e)
		{
			echo "SQL Query Error<br />";
			echo "Request: $SQL";
			echo $e->getMessage();
		}
		return $return;
	}

	// *** Methods *** //
	/**
	 * Returns the first result of the query
	 * @return mixed
	 */
	public static function getFirst($SQL)
	{
		$req = self::getDb()->query($SQL);
		$data = $req->fetch();
		return $data[0];
	}
	/**
	* Returns the first array of occuring data for the SQL query
	* @return array(mixed)
	*/
	public static function getLine($SQL)
	{
		$req = self::getDb()->query($SQL);
		return $req->fetch(PDO::FETCH_ASSOC);
	}
	
	/**
	 * Returns the last inserted ID in the current session
	 * @return int
	 */
	public static function lastInsertedId()
	{
		return self::getDb()->lastInsertId();
	}
	
	/**
	 * 
	 * @param string $SQL
	 * @return PDO_Statement
	 */
	public static function query($SQL)
	{
		$req = null;
		try
		{
			$req = self::getDb()->query($SQL);
		}
		catch(Exception $e)
		{
			echo "SQL Query Error<br />";
			echo "Request: $SQL";
			echo $e->getMessage();
		}
		return $req;
	}
}
?>