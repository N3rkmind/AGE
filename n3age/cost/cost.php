<?php

	include_once "../CDB.php";

	class CCost
	{
		public static function ChargeCost($jobId,$userId)
		{
			$db = CDB::getDb();
			// CHECK COST
			$req = $db->prepare("SELECT user_resource.amount AS my_amn,cost.amount,cost.resource_id FROM user_resource INNER JOIN cost ON user_resource.resource_id=cost.resource_id AND cost.job_id=:jobId WHERE user_resource.user_id=:userId");
			$req->bindParam(':jobId', $jobId);
			$req->bindParam(':userId', $userId);
			$req->execute();
			$c = $req->fetch(PDO::FETCH_ASSOC);

			if($c['my_amn'] < $c['amount'])
			{
				echo "8|Not enough resources";
				die;
			}

			$newAmn = $c['my_amn'] - $c['amount'];

			// CHARGE COST
			$req = $db->prepare("UPDATE user_resource SET amount=:newAmn WHERE resource_id=:resourceId AND user_id=:user_Id");
			$req->bindParam(':newAmn', $newAmn);
			$req->bindParam(':resourceId', $c['resource_id']);
			$req->bindParam(':user_Id', $userId);
			$r = $req->execute();

			return $r;
		}
	}

?>