<?php 

	include_once('../CDB.php');

	// Select Buildings
	$db = CDB::getDb();
	$req = $db->prepare("SELECT * FROM cost");
	$req->execute();

	$cost = $req->fetchAll(PDO::FETCH_ASSOC);
	if(empty($cost))
	{
		echo "1 - Unable to fetch costs";
		die;
	}

	echo json_encode($cost);

?>