<?php 

	include_once('CDB.php');

	if(empty($_POST['key']))
	{
		echo "-1|Missing key";
		die;
	}
	$key = $_POST['key'];

	// Getting userId from Key
	$db = CDB::getDb();
	$req = $db->prepare("SELECT id FROM user where access_token=:key");
	$req->bindParam(':key', $key);
	$req->execute();
	$userId = $req->fetch(PDO::FETCH_ASSOC)['id'];
?>