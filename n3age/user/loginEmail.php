<?php

	include_once "../CDB.php";
	include_once "../helper/CToken.php";

	$email = null;
	$password = null;

	if(empty($_POST['email']))
	{
		echo "1|Email is empty";
		die;
	}
	$email = $_POST['email'];

	foreach (getallheaders() as $name => $value) 
	{
	    if($name == "Auth")
	    {
	    	$password = $value;
	    }
	}

	if(empty($password))
	{
		echo "2|password is empty";
		die;
	}

	// Pulling user from DB
	$db = CDB::getDb();
	$req = $db->prepare("SELECT * FROM user where email=:email");
	$req->bindParam(':email', $email);
	$req->execute();
	$row = $req->fetch(PDO::FETCH_ASSOC);

	if(empty($row))
	{
		echo '3|'.$email.' does not exist.';
		die;
	}
	if($row['password'] != $password)
	{
		echo '4|Wrong password';
		die;
	}

	$token = CToken::generateToken($row['email']);
	$userId = $row['id'];

	// Sending the token to the client / DB
	$req = $db->prepare("UPDATE user SET access_token=:key where id=:userId");
	$req->bindParam(':key', $token);
	$req->bindParam(':userId', $userId);
	$req->execute();

	if(!$req)
	{
		echo "5|Error updating token";
		die;
	}

	echo "cnn|email|" . $row['username'] . '|' . $userId . '|' . $token;
?>