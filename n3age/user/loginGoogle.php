<?php

	include_once "../CDB.php";
	include_once "../helper/CToken.php";

	$email = null;
	$token = null;

	if(empty($_POST['email']))
	{
		echo "1|email is empty";
		die;
	}
	$email = $_POST['email'];

	// Pulling user from DB
	$db = CDB::getDb();
	$req = $db->prepare("SELECT * FROM user where email=:email");
	$req->bindParam(':email', $email);
	$req->execute();
	$row = $req->fetch(PDO::FETCH_ASSOC);

	if(empty($row))
	{
		CreateUser($email);
	}
	else
	{
		// Sign in
		if($row['id'] == $row['username'])
		{
			CreateUser($email);
			return;
		}

		SignIn($row['username'],$row['email']);
	}

	function SignIn($username,$email)
	{
		$token = CToken::generateToken($row['email']);
		$userId = $row['id'];

		// Sending the token to the client / DB
		$db = CDB::getDb();
		$req = $db->prepare("UPDATE user SET access_token=:key where id=:userId");
		$req->bindParam(':key', $token);
		$req->bindParam(':userId', $userId);
		$req->execute();

		if(!$req)
		{
			echo "3|Error updating token";
			die;
		}

		echo "cnn|google|" . $row['username'] . '|' . $userId . '|' . $token;
	}

	function CreateUser($email)
	{
		// Partial account creation
		$token = CToken::generateToken($email);

		$db = CDB::getDb();
		$req = $db->prepare("INSERT INTO user (email,username,password,access_token) VALUES (:email,'username','0',:token)");
		$req->bindParam(':email', $email);
		$req->bindParam(':token', $token);
		$req->execute();

		if(!$req)
		{
			echo "2|Error creating partial account";
			die;
		}

		$lastId = CDB::lastInsertedId();

		// Set username to id
		$req = $db->prepare("UPDATE user SET username=:username WHERE id=:username");
		$req->bindParam(':username', $lastId);
		$req->execute();

		echo "crt|google|" . $email . "|" . $token;
	}

?>