<?php

	include_once "../CDB.php";
	
	if(empty($_POST['email']))
	{
		echo "1 - email is empty";
		die;
	}
	$email = $_POST['email'];

	// Select User
	$db = CDB::getDb();
	$req = $db->prepare("SELECT * FROM user WHERE email=:email");
	$req->bindParam(':email', $email);
	$req->execute();
	$res = $req->fetch(PDO::FETCH_ASSOC);

	if(!empty($res))
	{
		echo "2 - User already exist";
		die;
	}

	echo "0 - Email available";
?>