<?php

	include_once "../CDB.php";

	if(empty($_POST['email']))
	{
		echo "1 - email is empty";
		die;
	}

	if(empty($_POST['displayName']))
	{
		echo "2 - display name is empty";
		die;
	}

	$password = "";
	foreach (getallheaders() as $name => $value) 
	{
	    if($name == "Auth")
	    {
	    	$password = $value;
	    }
	}

	if(empty($password))
	{
		echo "3 - Password is empty";
		die;
	}

	$email = $_POST['email'];
	$username = $_POST['displayName'];

	// Select User
	$db = CDB::getDb();
	$req = $db->prepare("SELECT * FROM user WHERE email=:email");
	$req->bindParam(':email', $email);
	$req->execute();
	$user = $req->fetch(PDO::FETCH_ASSOC);
	if(!empty($user))
	{
		echo "4 - User already exist";
		die;
	}

	// Creating User
	$req = $db->prepare("INSERT INTO user (email,username,password) VALUES (:email,:username,:password)");
	$req->bindParam(':email', $email);
	$req->bindParam(':username', $username);
	$req->bindParam(':password', $password);
	$req->execute();

	if(!$req)
	{
		echo "5 - error creating the account";
		die;
	}

	$userId = CDB::lastInsertedId();

	// Creating Resources
	// GetAllResources
	$req = $db->prepare("SELECT * FROM resource");
	$req->execute();
	$resources = $req->fetchAll(PDO::FETCH_ASSOC);

	for ($i=0; $i < count($resources); $i++) 
	{ 
		$resourceId = $resources[$i]['id'];
		$req = $db->prepare("INSERT INTO user_resource (user_id,resource_id,amount) VALUES (:userId,:resourceId,999)");
		$req->bindParam(':userId', $userId);
		$req->bindParam(':resourceId', $resourceId);
		$req->execute();
	}

	if(!$req)
	{
		echo "6 - error creating grid";
		die;
	}

	echo "0 - Success";
?>