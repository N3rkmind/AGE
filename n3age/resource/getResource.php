<?php

	include "../CDB.php";

	if(empty($_POST['userId']))
	{
		echo "1|Userid is empty";
		die;
	}
	$userId = $_POST['userId'];

	// Fetching the resources
	$db = CDB::getDb();
	$req = $db->prepare("SELECT * FROM user_resource WHERE user_id=:userId");
	$req->bindParam(':userId', $userId);
	$req->execute();
	$res = $req->fetchAll(PDO::FETCH_ASSOC);

	if(!$req )
	{
		echo "1|Unable to select user resource";
	}

	if(empty($res))
	{
		echo "2|resources response is empty";
	}

	$responseString = "";
	for ($i=0; $i < count($res); $i++)
	{ 
		$responseString .= $res[$i]['resource_id'] . '%' . $res[$i]['amount'] . '|';
	}
	$responseString = substr($responseString, 0, -1);
	
	echo "0|" . $responseString;
?>