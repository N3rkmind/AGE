<?php

	include_once "../CDB.php";

	class CResource
	{
		public static function GiveResource($rewardId,$userId)
		{
			$db = CDB::getDb();
			// CHECK COST
			$req = $db->prepare("SELECT user_resource.amount,reward.param,reward.target_id FROM user_resource INNER JOIN reward ON user_resource.resource_id=reward.target_id AND reward.id=:rewardId WHERE user_resource.user_id=:userId AND reward.target_table='resource'");
			$req->bindParam(':rewardId', $rewardId);
			$req->bindParam(':userId', $userId);
			$req->execute();
			$c = $req->fetch(PDO::FETCH_ASSOC);

			$newAmn = $c['amount'] + $c['param'];

			// CHARGE COSTn
			$req = $db->prepare("UPDATE user_resource SET amount=:newAmn WHERE resource_id=:resourceId AND user_id=:user_Id");
			$req->bindParam(':newAmn', $newAmn);
			$req->bindParam(':resourceId', $c['target_id']);
			$req->bindParam(':user_Id', $userId);
			$r = $req->execute();

			return $r;
		}

		public static function RemoveResource($costId,$userId)
		{
			$db = CDB::getDb();
			// CHECK COST
			$req->bindParam(':costId', $costId);
			$req->bindParam(':userId', $userId);
			$req->execute();
			$req = $db->prepare("SELECT user_resource.amount,cost.resource_amn,cost.resource_id FROM user_resource INNER JOIN cost ON user_resource.resource_id=cost.resource_id AND cost.id=:costId WHERE user_resource.user_id=:userId");
			$c = $req->fetch(PDO::FETCH_ASSOC);

			if($c['amount'] < $c['resource_amn'])
			{
				echo "8|Not enough resources";
				die;
			}

			$newAmn = $c['amount'] - $c['resource_amn'];

			// CHARGE COST
			$req = $db->prepare("UPDATE user_resource SET amount=:newAmn WHERE resource_id=:resourceId AND user_id=:user_Id");
			$req->bindParam(':newAmn', $newAmn);
			$req->bindParam(':resourceId', $c['resource_id']);
			$req->bindParam(':user_Id', $userId);
			$r = $req->execute();

			return $r;
		}
	}

?>