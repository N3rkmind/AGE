<?php 

	include_once('../CDB.php');

	// Select Buildings
	$db = CDB::getDb();
	$req = $db->prepare("SELECT * FROM reward");
	$req->execute();

	$rew = $req->fetchAll(PDO::FETCH_ASSOC);
	if(empty($rew))
	{
		echo "1 - Unable to fetch reward";
		die;
	}

	echo json_encode($rew);

?>