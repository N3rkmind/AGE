-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2017 at 06:11 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `n3age`
--

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `size_x` int(11) NOT NULL,
  `size_y` int(11) NOT NULL,
  `model_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`id`, `name`, `size_x`, `size_y`, `model_id`) VALUES
(1010, 'Lumber Mill (lv1)', 2, 2, 1010),
(1000, 'Farm (lv1)', 2, 2, 1000),
(1001, 'Farm (lv2)', 2, 2, 1001),
(1011, 'Lumber Mill (lv2)', 2, 2, 1011),
(1020, 'Mining Shack (lv1)', 2, 2, 1020),
(1021, 'Mining Shack (lv2)', 2, 2, 1021),
(1030, 'Gold Mine (lv1)', 2, 2, 1030),
(1031, 'Gold Mine (lv2)', 2, 2, 1031);

-- --------------------------------------------------------

--
-- Table structure for table `cost`
--

CREATE TABLE `cost` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `job_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cost`
--

INSERT INTO `cost` (`id`, `name`, `job_id`, `resource_id`, `amount`, `duration`) VALUES
(1, '(F) Build Farm lv1', 1000, 1, 10, 5),
(2, '(W) Build Farm lv1', 1000, 2, 25, 0),
(3, '(F) Build Lumber Mill lv1', 1010, 1, 20, 5),
(4, '(W) Build Lumber Mill lv1', 1010, 2, 10, 0),
(5, '(F) Build Mining Shack lv1', 1020, 1, 15, 5),
(6, '(W) Build Mining Shack lv1', 1020, 2, 30, 0),
(7, '(F) Build Gold Mine lv1', 1030, 1, 20, 5),
(8, '(W) Build Gold Mine lv1', 1030, 2, 40, 0),
(5000, 'Gather Food lv1', 3000, 1, 0, 5),
(5001, '(F) Upgrade To Farm lv2', 2000, 1, 50, 5),
(5002, '(W) Upgrade To Farm lv2', 2000, 2, 150, 0),
(5003, '(G) Upgrade To Farm lv2', 2000, 4, 50, 0),
(5004, '(F) Upgrade To Lumber Mill lv2', 2001, 1, 75, 5),
(5005, '(W) Upgrade To Lumber Mill lv2', 2001, 2, 150, 0),
(5006, '(G) Upgrade To Lumber Mill lv2', 2001, 4, 30, 0),
(5007, '(F) Upgrade to Mining Shack lv2', 2002, 1, 125, 5),
(5008, '(W) Upgrade to Mining Shack lv2', 2002, 2, 125, 0),
(5009, '(F) Upgrade to Gold Mine lv2', 2003, 1, 200, 5),
(5010, '(W) Upgrade to Gold Mine lv2', 2003, 2, 350, 0),
(5011, 'Gather Wood lv1', 3010, 1, 0, 5),
(5012, 'Gather Stone lv1', 3020, 1, 0, 5),
(5013, 'Gather Gold lv1', 3030, 1, 0, 5),
(5014, 'Gather Food lv2', 3001, 1, 0, 5),
(5015, 'Gather Wood lv2', 3011, 1, 0, 5),
(5016, 'Gather Stone lv2', 3021, 1, 0, 5),
(5017, 'Gather Gold lv2', 3031, 1, 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE `job` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `type` varchar(32) NOT NULL,
  `caller_table` varchar(32) NOT NULL,
  `caller_data_id` int(11) NOT NULL,
  `target_table` varchar(32) NOT NULL,
  `reward_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job`
--

INSERT INTO `job` (`id`, `name`, `type`, `caller_table`, `caller_data_id`, `target_table`, `reward_id`) VALUES
(1000, 'Building Farm (lv1)', 'build', 'building', 1000, 'building', 100),
(3000, 'Gathering Food (lv1)', 'gather', 'building', 1000, 'resource', 3000),
(2000, 'Upgrade to Farm lv2', 'upgrade', 'building', 1000, 'building', 2000),
(1010, 'Build Lumber Mill (lv1)', 'build', 'building', 1010, 'building', 100),
(1020, 'Build Mining Shack (lv1)', 'build', 'building', 1020, 'building', 100),
(1030, 'Build Gold Mine (lv1)', 'build', 'building', 1030, 'building', 100),
(2001, 'Upgrade to Lumber Mill lv2', 'upgrade', 'building', 1010, 'building', 2001),
(2002, 'Upgrade to Mining Shack lv2', 'upgrade', 'building', 1020, 'building', 2002),
(2003, 'Upgrade to Gold Mine lv2', 'upgrade', 'building', 1030, 'building', 2003),
(3010, 'Gathering Wood (lv1)', 'gather', 'building', 1010, 'resource', 3010),
(3020, 'Gathering Stone (lv1)', 'gather', 'building', 1020, 'resource', 3020),
(3030, 'Gathering Gold (lv1)', 'gather', 'building', 1030, 'resource', 3030),
(3001, 'Gathering Food (lv2)', 'gather', 'building', 1001, 'resource', 3001),
(3011, 'Gathering Wood (lv2)', 'gather', 'building', 1011, 'resource', 3011),
(3021, 'Gathering Stone (lv2)', 'gather', 'building', 1021, 'resource', 3021),
(3031, 'Gathering Gold (lv2)', 'gather', 'building', 1031, 'resource', 3031);

-- --------------------------------------------------------

--
-- Table structure for table `resource`
--

CREATE TABLE `resource` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resource`
--

INSERT INTO `resource` (`id`, `name`) VALUES
(1, 'Food'),
(2, 'Wood'),
(3, 'Stone'),
(4, 'Gold');

-- --------------------------------------------------------

--
-- Table structure for table `reward`
--

CREATE TABLE `reward` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `target_table` varchar(32) NOT NULL,
  `target_id` int(11) NOT NULL,
  `param` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reward`
--

INSERT INTO `reward` (`id`, `name`, `target_table`, `target_id`, `param`) VALUES
(100, 'Complete build', 'building', 0, '0'),
(3000, 'Gather Food (lv1)', 'resource', 1, '25'),
(3010, 'Gather Wood (lv1)', 'resource', 2, '20'),
(3020, 'Gather Stone (lv1)', 'resource', 3, '20'),
(3030, 'Gather Gold (lv1)', 'resource', 4, '15'),
(2000, 'Upgrade Farm lv2 ', 'building', 1001, ''),
(2001, 'Upgrade Lumber Mill lv2 ', 'building', 1011, ''),
(2002, 'Upgrade Mining Shack lv2 ', 'building', 1021, ''),
(2003, 'Upgrade Gold Mine lv2 ', 'building', 1031, ''),
(3001, 'Gather Food (lv2)', 'resource', 1, '50'),
(3011, 'Gather Wood (lv2)', 'resource', 2, '40'),
(3021, 'Gather Stone (lv2)', 'resource', 3, '40'),
(3031, 'Gather Gold (lv2)', 'resource', 4, '30');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_update` datetime DEFAULT NULL,
  `access_token` varchar(64) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`, `last_update`, `access_token`) VALUES
(83, '123', '123', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', NULL, 'fb8f5e91cb2b82f134a6e2eeed00f2586b936a3073a13bdbaa0035a959f780f9');

-- --------------------------------------------------------

--
-- Table structure for table `user_building`
--

CREATE TABLE `user_building` (
  `id` int(11) NOT NULL,
  `data_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pos_x` int(11) NOT NULL,
  `pos_y` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_building`
--

INSERT INTO `user_building` (`id`, `data_id`, `user_id`, `pos_x`, `pos_y`) VALUES
(601, 1011, 83, 5, 3),
(602, 1001, 83, 5, 5),
(597, 1001, 83, 10, 6),
(600, 1031, 83, 7, 3),
(603, 1011, 83, 7, 5),
(604, 1001, 83, 9, 3),
(605, 1021, 83, 7, 1),
(606, 1001, 83, 3, 3),
(607, 1011, 83, 5, 1),
(608, 1031, 83, 9, 1),
(609, 1031, 83, 11, 1),
(610, 1031, 83, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_job`
--

CREATE TABLE `user_job` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `caller_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `start_time` int(11) NOT NULL,
  `completed` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_job`
--

INSERT INTO `user_job` (`id`, `user_id`, `job_id`, `caller_id`, `target_id`, `start_time`, `completed`) VALUES
(958, 83, 2001, 589, 589, 1485340498, b'1'),
(965, 83, 2000, 590, 590, 1485340597, b'1'),
(966, 83, 3001, 590, 590, 1485340602, b'1'),
(967, 83, 3001, 590, 590, 1485340609, b'1'),
(968, 83, 3001, 590, 590, 1485340638, b'1'),
(969, 83, 3001, 590, 590, 1485340644, b'0'),
(972, 83, 2001, 591, 591, 1485341575, b'0'),
(973, 83, 2000, 592, 592, 1485341576, b'1'),
(974, 83, 3001, 592, 592, 1485341582, b'0'),
(976, 83, 2001, 593, 593, 1485344016, b'0'),
(977, 83, 1010, 594, 594, 1485354453, b'1'),
(978, 83, 3010, 594, 594, 1485354459, b'0'),
(979, 83, 1010, 595, 595, 1485354658, b'1'),
(981, 83, 2001, 595, 595, 1485354665, b'1'),
(982, 83, 3011, 595, 595, 1485354670, b'1'),
(983, 83, 1010, 596, 596, 1485354724, b'1'),
(988, 83, 2000, 597, 597, 1485354746, b'1'),
(987, 83, 2001, 596, 596, 1485354745, b'1'),
(989, 83, 3011, 596, 596, 1485354751, b'1'),
(990, 83, 3001, 597, 597, 1485354752, b'1'),
(991, 83, 3011, 596, 596, 1485354836, b'1'),
(992, 83, 3001, 597, 597, 1485354837, b'1'),
(993, 83, 3001, 597, 597, 1485354842, b'1'),
(994, 83, 3011, 595, 595, 1485354844, b'1'),
(995, 83, 3011, 596, 596, 1485354845, b'1'),
(996, 83, 3011, 595, 595, 1485356579, b'1'),
(997, 83, 3011, 596, 596, 1485356581, b'1'),
(998, 83, 3001, 597, 597, 1485356582, b'1'),
(1004, 83, 2001, 598, 598, 1485875023, b'1'),
(1001, 83, 1010, 599, 599, 1485362329, b'1'),
(1008, 83, 2001, 599, 599, 1485875036, b'1'),
(1005, 83, 3011, 598, 598, 1485875029, b'1'),
(1006, 83, 3011, 595, 595, 1485875031, b'1'),
(1007, 83, 3011, 598, 598, 1485875035, b'0'),
(1009, 83, 3001, 597, 597, 1485875056, b'1'),
(1010, 83, 3011, 596, 596, 1485875058, b'0'),
(1011, 83, 3011, 599, 599, 1485875059, b'0'),
(1012, 83, 3011, 595, 595, 1485875060, b'0'),
(1013, 83, 3001, 597, 597, 1485875068, b'1'),
(1014, 83, 1030, 600, 600, 1485875080, b'1'),
(1016, 83, 2003, 600, 600, 1485875091, b'1'),
(1017, 83, 3031, 600, 600, 1485875096, b'1'),
(1018, 83, 3031, 600, 600, 1485875159, b'1'),
(1019, 83, 3031, 600, 600, 1485875625, b'1'),
(1020, 83, 3001, 597, 597, 1485875629, b'1'),
(1023, 83, 2001, 601, 601, 1485875639, b'1'),
(1025, 83, 2000, 602, 602, 1485875642, b'1'),
(1026, 83, 3011, 601, 601, 1485875645, b'1'),
(1027, 83, 3001, 602, 602, 1485875648, b'1'),
(1028, 83, 3001, 597, 597, 1485875665, b'1'),
(1029, 83, 3031, 600, 600, 1485875666, b'1'),
(1030, 83, 3011, 601, 601, 1485875667, b'1'),
(1031, 83, 3001, 602, 602, 1485875671, b'1'),
(1032, 83, 3031, 600, 600, 1485875671, b'1'),
(1033, 83, 1010, 603, 603, 1485875698, b'1'),
(1034, 83, 1000, 604, 604, 1485875703, b'1'),
(1037, 83, 2001, 603, 603, 1485875710, b'1'),
(1038, 83, 2000, 604, 604, 1485875711, b'1'),
(1040, 83, 2002, 605, 605, 1485875723, b'1'),
(1041, 83, 3021, 605, 605, 1485875728, b'1'),
(1042, 83, 1000, 606, 606, 1485875813, b'1'),
(1043, 83, 1010, 607, 607, 1485875819, b'1'),
(1044, 83, 3010, 607, 607, 1485875824, b'1'),
(1052, 83, 2000, 606, 606, 1485970205, b'1'),
(1046, 83, 3021, 605, 605, 1485970199, b'1'),
(1047, 83, 3001, 604, 604, 1485970200, b'1'),
(1048, 83, 3031, 600, 600, 1485970200, b'1'),
(1051, 83, 2001, 607, 607, 1485970203, b'1'),
(1050, 83, 3011, 601, 601, 1485970202, b'0'),
(1053, 83, 3001, 602, 602, 1485970207, b'0'),
(1054, 83, 3011, 603, 603, 1485970208, b'1'),
(1055, 83, 3001, 597, 597, 1485970209, b'1'),
(1056, 83, 1030, 608, 608, 1485970216, b'1'),
(1057, 83, 3001, 606, 606, 1485970258, b'0'),
(1058, 83, 3011, 607, 607, 1485970259, b'1'),
(1059, 83, 1030, 609, 609, 1486187863, b'1'),
(1060, 83, 3011, 607, 607, 1486187865, b'1'),
(1061, 83, 3021, 605, 605, 1486187866, b'1'),
(1064, 83, 2003, 608, 608, 1486187869, b'1'),
(1063, 83, 3030, 609, 609, 1486187868, b'1'),
(1065, 83, 3001, 604, 604, 1486187871, b'1'),
(1066, 83, 3031, 600, 600, 1486187872, b'1'),
(1067, 83, 3011, 603, 603, 1486187873, b'0'),
(1068, 83, 3001, 597, 597, 1486187874, b'1'),
(1070, 83, 2003, 609, 609, 1486187887, b'1'),
(1071, 83, 3031, 608, 608, 1486187889, b'1'),
(1072, 83, 3021, 605, 605, 1486975159, b'0'),
(1073, 83, 3001, 597, 597, 1487087673, b'1'),
(1074, 83, 3001, 597, 597, 1487087678, b'0'),
(1075, 83, 3031, 609, 609, 1487087686, b'0'),
(1076, 83, 3031, 608, 608, 1487087687, b'1'),
(1077, 83, 3001, 604, 604, 1487087687, b'1'),
(1078, 83, 3031, 608, 608, 1487087692, b'0'),
(1079, 83, 3001, 604, 604, 1487087693, b'0'),
(1080, 83, 3031, 600, 600, 1487087694, b'0'),
(1081, 83, 3011, 607, 607, 1487087695, b'0'),
(1082, 83, 1030, 610, 610, 1487087700, b'1'),
(1084, 83, 2003, 610, 610, 1487087713, b'1'),
(1085, 83, 3031, 610, 610, 1487087719, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `user_resource`
--

CREATE TABLE `user_resource` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_resource`
--

INSERT INTO `user_resource` (`id`, `user_id`, `resource_id`, `amount`) VALUES
(181, 83, 1, 1574),
(182, 83, 2, 1579),
(183, 83, 3, 1119),
(184, 83, 4, 1284);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reward`
--
ALTER TABLE `reward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_building`
--
ALTER TABLE `user_building`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_job`
--
ALTER TABLE `user_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_resource`
--
ALTER TABLE `user_resource`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `building`
--
ALTER TABLE `building`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1032;
--
-- AUTO_INCREMENT for table `cost`
--
ALTER TABLE `cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5019;
--
-- AUTO_INCREMENT for table `job`
--
ALTER TABLE `job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3032;
--
-- AUTO_INCREMENT for table `resource`
--
ALTER TABLE `resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `reward`
--
ALTER TABLE `reward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3032;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `user_building`
--
ALTER TABLE `user_building`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=611;
--
-- AUTO_INCREMENT for table `user_job`
--
ALTER TABLE `user_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1086;
--
-- AUTO_INCREMENT for table `user_resource`
--
ALTER TABLE `user_resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
