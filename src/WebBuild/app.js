var host = "http://n3k.ca/n3age";
//var host = "http://127.0.0.1/n3age";

var config = 
{
	apiKey: "AIzaSyBFCs1G_C46MK_oL-fcEnylfSipQuOFknA",
	authDomain: "n3age-150516.firebaseapp.com",
	databaseURL: "https://n3age-150516.firebaseio.com",
	storageBucket: "n3age-150516.appspot.com",
	messagingSenderId: "380350807440"
};

firebase.initializeApp(config);

function OnGoogleButton()
{
	var provider = new firebase.auth.GoogleAuthProvider();
	firebase.auth().signInWithPopup(provider).then(function(result)
	{
		var token = result.credential.accessToken;	  // This gives you a Google Access Token. You can use it to access the Google API.
		var user = result.user;	  // The signed-in user info.
		console.log(result);
		var params = "email=" + result.user.email;
		Ajax(host + "/user/loginGoogle.php",SendToGame,params);
	}).catch(function(error)
	{
		var errorCode = error.code;
		var errorMessage = error.message;
		var email = error.email;
		var credential = error.credential;
		SendToGame ('error');
		console.log(error);
	});
};

function SendToGame(msg)
{
	SendMessage ('Web', 'OnWebMessage', msg);
};

function Ajax(target,callback,params) 
{
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() 
    {
		if (xmlhttp.readyState == XMLHttpRequest.DONE )
		{
			if (xmlhttp.status == 200) 
			{
			   callback(xmlhttp.responseText);
			}
			else if (xmlhttp.status == 400) 
			{
		  		alert('There was an error 400');
			}
			else
			{
		   		alert('something else other than 200 was returned');
			}
		}
    };

    xmlhttp.open("POST", target, true);

    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", params.length);
	xmlhttp.setRequestHeader("Connection", "close");

    xmlhttp.send(params);
};

function onPageLoad()
{
	var cv = document.getElementById("canvas");
	var ctx = cv.getContext('2d');
	ctx.canvas.width = window.innerWidth;
	ctx.canvas.height = window.innerHeight;
}