﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour
{
    public Text playerName;
    public Text playerGuild; 

    private void Start()
    {
        playerName.text = User.ThisUser.Username;
    }
}
