﻿using UnityEngine;
using LitJson;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

public class Tile
{
    public Tile() { }

    public int x;
    public int y;
    public int occupiedBy = -1;

    public override string ToString()
    {
        string r = "Tile x:" + x.ToString() + " y:" + y.ToString() + " occupied by : " + occupiedBy.ToString();
        return r;
    }
}

public class Grid : MonoSingleton<Grid>
{
    private const int SIZE_X = 15;
    private const int SIZE_Y = 10;
    private const float TILE_WIDTH_HEIGHT = 10.0f;

    private List<Building> buildings;

    private Camera cam;
    private LayerMask rayMask;
    private Building selectedBuilding;

    private Transform floorObject;
    public Material previewMaterial;
    private Transform previewObject;
    private int mouseoverX;
    private int mouseoverY;

    private WWW loadGridRequest;
    private WWWForm loadGridForm;
    private WWW addToGridRequest;
    private WWWForm addToGridForm;
    private WWW sellFromGridRequest;
    private WWWForm sellFromGridForm;

    public Tile[,] grid = new Tile[SIZE_X,SIZE_Y];
    private Tile hoveredTile;

    private Building previewBuilding;
    private GameObject previewBuildingObject;
    private bool isPreviewingBuilding;

    #region Init
    private void Start()
    {
        cam = Camera.main;
        rayMask = LayerMask.GetMask("Grid");

        // Floor layer
        floorObject = Helper.CreateFloorQuad().transform;
        floorObject.localScale = new Vector3(TILE_WIDTH_HEIGHT*SIZE_X,1,TILE_WIDTH_HEIGHT*SIZE_Y);
        floorObject.gameObject.layer = LayerMask.NameToLayer("Grid");
        floorObject.gameObject.AddComponent<BoxCollider>();
        floorObject.name = "Floor Object";
        floorObject.GetComponent<MeshRenderer>().enabled = false;

        // Preview Object
        // Unity plane ( *0.2f )
        previewObject = Helper.CreateFloorQuad().transform;
        previewObject.GetComponent<Renderer>().material = previewMaterial;
        previewObject.localScale = Vector3.one * TILE_WIDTH_HEIGHT;
        previewObject.name = "Preview Object";

        CreateGrid();
        RequestGrid();

    }
    private void RequestGrid()
    {
        loadGridForm = new WWWForm();
        loadGridForm.AddField("key", User.ThisUser.Key);

        loadGridRequest = new WWW(Web.gridLoad, loadGridForm);
        StartCoroutine(WaitForGridData(loadGridRequest));
    }
    private System.Collections.IEnumerator WaitForGridData(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Unable to load grid");
            Authentication.instance.Disconnect();
            yield break;
        }

        LoadGrid(www.text);
    }
    private void CreateGrid()
    {
        for (int i = 0; i < SIZE_X; i++)
        {
            for (int j = 0; j < SIZE_Y; j++)
            {
                grid[i, j] = new Tile() { x = i, y = j, occupiedBy = -1 };
            }
        }
    }
    #endregion

    private void Update()
    {
        UpdateMousePreview();

        if (isPreviewingBuilding)
        {
            UpdateBuildingPreview();
            if (Input.GetMouseButtonUp(0))
            {
                TrySpawnBuilding();
            }
        }
        else
        {
            // If not previewing, select
            if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                if (mouseoverX == -1)
                    return;

                if(grid[mouseoverX,mouseoverY].occupiedBy != -1)
                    SelectBuilding();
            }
        }

#if UNITY_EDITOR
        Debug.DrawRay(Vector3.zero, Vector3.right * SIZE_X * TILE_WIDTH_HEIGHT, Color.green);
        Debug.DrawRay(Vector3.zero, Vector3.forward * SIZE_Y * TILE_WIDTH_HEIGHT, Color.green);
        Debug.DrawRay(Vector3.right * SIZE_X * TILE_WIDTH_HEIGHT, Vector3.forward * SIZE_Y * TILE_WIDTH_HEIGHT, Color.green);
        Debug.DrawRay(Vector3.forward * SIZE_Y * TILE_WIDTH_HEIGHT, Vector3.right * SIZE_X * TILE_WIDTH_HEIGHT, Color.green);
#endif
    }
    private void UpdateMousePreview()
    {
        if (selectedBuilding != null)
            return;

        RaycastHit hit;
        int x = -1, y = -1;

        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 500, rayMask))
        {
            x = (int)(hit.point.x / TILE_WIDTH_HEIGHT);
            y = (int)(hit.point.z / TILE_WIDTH_HEIGHT);
        }

        if (mouseoverX != x || mouseoverY != y)
        {
            mouseoverX = x;
            mouseoverY = y;
            MouseOverChanged();
        }
    }
    private void UpdateBuildingPreview()
    {
        if (Input.GetMouseButtonDown(1))
            ResetPreviewBuilding();
    }
    public void UpdateBuildingAfterUpgrade(int activeId,int newDataId)
    {
        Building b = buildings.Find(x => x.ActiveId == activeId);
        Building newBuilding = Data.GetBuildingById(newDataId);
        b.UpgradeBuilding(newBuilding);
    }

    public Vector3 IndexToWorld(int x, int y)
    {
        return new Vector3(x * TILE_WIDTH_HEIGHT, 0, y * TILE_WIDTH_HEIGHT);
    }
    public void ResetGrid()
    {
        ResetPreviewBuilding();
    }
    private bool TestGridSpace(int x, int y, int sizeX, int sizeY)
    {
        if (x == -1)
        {
            Log.WriteLine(LogType.Warning, "Preview object is out of bounds");
            return false;
        }

        for (int i = 0; i < sizeX; i++)
        {
            for (int j = 0; j < sizeY; j++)
            {
                if (x + i >= SIZE_X || y + j >= SIZE_Y)
                {
                    Log.WriteLine(LogType.Warning, "Preview object is out of bounds");
                    return false;
                }
                if (grid[x+i, y+j].occupiedBy != -1)
                {
                    Log.WriteLine(LogType.Warning, "Preview object is overlapping occupied tile");
                    return false;
                }
            }
        }

        return true;
    }

    #region highlight
    private void SelectBuilding()
    {
        selectedBuilding = GetBuildingById(grid[mouseoverX, mouseoverY].occupiedBy);
        SelectionMenu.instance.selectionBuild.Show(selectedBuilding);
    }
    public void ClearSelection()
    {
        selectedBuilding = null;
    }
    private void MouseOverChanged()
    {
        if (hoveredTile != null)
        {
            // clear previous tile
        }

        if (mouseoverX != -1)
        {
            if(mouseoverX >= SIZE_X || mouseoverY >= SIZE_Y)
            {
                Log.WriteLine(LogType.Error, "Mouse over out of bounds!");
                return;
            }

            hoveredTile = grid[mouseoverX, mouseoverY];


            // If theres a building
            if (hoveredTile.occupiedBy != -1)
            {
                Building b = GetBuildingById(hoveredTile.occupiedBy);
                previewObject.transform.localScale = new Vector3(b.SizeX * TILE_WIDTH_HEIGHT, 1, b.SizeY * TILE_WIDTH_HEIGHT);
                previewObject.transform.position = IndexToWorld(b.PosX,b.PosY);
            }
            else
            {
                previewObject.transform.position = IndexToWorld(mouseoverX, mouseoverY);
                previewObject.transform.localScale = Vector3.one * TILE_WIDTH_HEIGHT;
            }
        }

        if (isPreviewingBuilding)
        {
            if (mouseoverX == -1)
                previewBuildingObject.transform.position = -Vector3.one * 100.0f; // Move it outside of FoV;
            else
            {
                previewObject.transform.localScale = new Vector3(previewBuilding.SizeX * TILE_WIDTH_HEIGHT, 1, previewBuilding.SizeY * TILE_WIDTH_HEIGHT);
                previewBuildingObject.transform.position = IndexToWorld(mouseoverX, mouseoverY);
            }
        }
    }
    #endregion

    #region building
    public void LoadGrid(string data)
    {
        string[] vData = data.Split('|');
        JsonData builds = JsonMapper.ToObject(new JsonReader(vData[1]));

        buildings = new List<Building>();

        for (int i = 0; i < builds.Count; i++)
        {
            ResetPreviewBuilding();
            previewBuilding = new Building(Data.GetBuildingById(int.Parse((string)builds[i]["data_id"])));
            previewBuilding.ActiveId = int.Parse((string)builds[i]["id"]);
            SpawnBuilding(int.Parse((string)builds[i]["pos_x"]),int.Parse((string)builds[i]["pos_y"]));
        }
    }
    public void PreviewBuilding(int index)
    {
        if (previewBuildingObject != null)
            ResetPreviewBuilding();

        previewBuilding = Data.GetBuildingById(index);
        previewBuildingObject = previewBuilding.CreateVisual();
        previewBuilding.Go = previewBuildingObject;
        isPreviewingBuilding = true;
    }
    private void ResetPreviewBuilding()
    {
        previewBuilding = null;
        Destroy(previewBuildingObject);
        isPreviewingBuilding = false;
    }
    public Building GetBuildingById(int index)
    {
        var b = buildings.Find(x => x.ActiveId == index);
        if (b == null)
            Log.WriteLine(LogType.Error, "Unable to find active building : " + index.ToString());
        return b;
    }
    private void TrySpawnBuilding()
    {
        // Client side check
        if (!TestGridSpace(mouseoverX,mouseoverY,previewBuilding.SizeX,previewBuilding.SizeY))
        {
            Log.WriteLine(LogType.Error, "Unable to place building here");
            return;
        }

        // Server side check
        addToGridForm = new WWWForm();
        addToGridForm.AddField("key", User.ThisUser.Key);
        addToGridForm.AddField("buildingId", previewBuilding.DataId);
        addToGridForm.AddField("pos", mouseoverX.ToString() + '|' + mouseoverY.ToString());

        addToGridRequest = new WWW(Web.gridAdd, addToGridForm);
        StartCoroutine(WaitForSpawnRequest(addToGridRequest));
    }
    private System.Collections.IEnumerator WaitForSpawnRequest(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Unable to add building");
            yield break;
        }

        Log.WriteLine(LogType.Information, www.text);

        switch (www.text[0])
        {
            case '0':
                // Able to create
                string[] data = www.text.Split('|');
                previewBuilding.ActiveId = int.Parse(data[1]);
                JobManager.instance.StartBuildJob(previewBuilding.DataId, previewBuilding.ActiveId, int.Parse(data[2]));
                SpawnBuilding(mouseoverX,mouseoverY);
                break;
            case '8':
                Log.WriteLine(LogType.Error, "Not enough resources");
                break;
            default:
                Log.WriteLine(LogType.Error, "Unable to spawn building");
                break;
        }

        yield break;
    }
    private void SpawnBuilding(int x,int y)
    {
        previewBuilding.SetGridPosition(x, y);

        // Create visual
            // Is it currently building?
        previewBuilding.Go = previewBuilding.CreateVisual(true);

        previewBuilding.Go.transform.position = IndexToWorld(x, y);

        // Update tiles
        for (int i = 0; i < previewBuilding.SizeX; i++)
            for (int j = 0; j < previewBuilding.SizeY; j++)
                grid[x + i, y + j].occupiedBy = previewBuilding.ActiveId;

        buildings.Add(new Building(previewBuilding));
        ResetPreviewBuilding();
    }
    public void SellFromGrid(Building b)
    {
        sellFromGridForm = new WWWForm();
        sellFromGridForm.AddField("key", User.ThisUser.Key);
        sellFromGridForm.AddField("buildingId", b.ActiveId);

        sellFromGridRequest = new WWW(Web.gridSell, sellFromGridForm);
        StartCoroutine(WaitForSellRequest(sellFromGridRequest));
    }
    private System.Collections.IEnumerator WaitForSellRequest(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Unable to reach server");
            yield break;
        }

        Log.WriteLine(LogType.Information, www.text);
        switch (www.text[0])
        {
            case '0':
                Log.WriteLine(LogType.Good, "Deleted Building!");
                Building b = GetBuildingById(int.Parse(www.text.Split('|')[1]));
                DeleteBuilding(b);
                Job j = JobManager.instance.GetJob(b);
                JobManager.instance.RemoveJob(j);
                break;
            default:
                Log.WriteLine(LogType.Error, "Unable to delete Building!");
                break;
        }
    }
    private void DeleteBuilding(Building b)
    {
        buildings.Remove(b);

        for (int i = 0; i < b.SizeX; i++)
        {
            for (int j = 0; j < b.SizeY; j++)
            {
                grid[b.PosX + i, b.PosY + j].occupiedBy = -1;
            }
        }

        Destroy(b.Go);
    }
    #endregion
}
