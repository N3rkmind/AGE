﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour
{
    private const float MIN_X = -50.0f;
    private const float MIN_Y = 50.0f;
    private const float MIN_Z = -50.0f;
    private const float MAX_X = 150.0f;
    private const float MAX_Y = 150.0f;
    private const float MAX_Z = 100.0f;

    private const float SCROLL_RECTANGLE_SIZE = 50.0f;

    private float sensivityX = 50.0f;
    private float sensivityY = 50.5f;
    private float sensivityZ = 20.5f;

    private Vector3 desiredPosition;
    private float distance;
    private Vector3 lastMousePosition;

    private void Start()
    {
        transform.position = new Vector3(1,10.0f,1);
        desiredPosition = transform.position;
    }

    private void Update()
    {
        ScrollInput();
        ClampCamera();

        transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * 50.0f);
    }

    private void ScrollInput()
    {
        Vector3 mousePos = Input.mousePosition;

        if (Input.GetMouseButton(2))
        {
            Vector3 delta = Vector3.zero;
            delta.x = -(mousePos.x - lastMousePosition.x) * sensivityX * Time.deltaTime;
            delta.z = -(mousePos.y - lastMousePosition.y) * sensivityZ * Time.deltaTime;
            desiredPosition += delta;
        }
        else
        {
            if (mousePos.x < SCROLL_RECTANGLE_SIZE)
                desiredPosition -= Vector3.right * sensivityX * Time.deltaTime;
            else if (mousePos.x > Screen.width - SCROLL_RECTANGLE_SIZE)
                desiredPosition += Vector3.right * sensivityX * Time.deltaTime;

            if (mousePos.y < SCROLL_RECTANGLE_SIZE)
                desiredPosition -= Vector3.forward * sensivityZ * Time.deltaTime;
            else if (mousePos.y > Screen.height - SCROLL_RECTANGLE_SIZE)
                desiredPosition += Vector3.forward * sensivityZ * Time.deltaTime;
        }

        desiredPosition.y -= Input.mouseScrollDelta.y * sensivityY * Time.deltaTime;
        lastMousePosition = mousePos;
    }

    private void ClampCamera()
    {
        desiredPosition.x = Mathf.Clamp(desiredPosition.x, MIN_X, MAX_X);
        desiredPosition.y = Mathf.Clamp(desiredPosition.y, MIN_Y, MAX_Y);
        desiredPosition.z = Mathf.Clamp(desiredPosition.z, MIN_Z, MAX_Z);
    }
}
