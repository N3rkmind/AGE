﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum ActionBarButton
{
    Build = 0,
    Research = 1,
    WorldMap = 2,
    Shop = 3,
    Guild = 4
}

public class ActionBar : MonoBehaviour
{
    private Transform actionBarTransform;
    private Button[] actionButtons;

    private Transform menuContainer;
    private CanvasGroup menuGroup;

    private int currentMenuIndex = -1;

    private void Start()
    {
        actionBarTransform = GameObject.Find("ActionBar").transform;
        actionButtons = GetComponentsInChildren<Button>();

        foreach(Button b in actionButtons)
            b.transform.GetChild(0).gameObject.SetActive(false);

        menuContainer = GameObject.Find("Menus").transform;
        menuGroup = menuContainer.GetComponent<CanvasGroup>();

        CloseMenu();
    }

    #region menus
    public void OpenMenu(int index)
    {
        menuGroup.alpha = 1;
        menuGroup.interactable = true;
        menuGroup.blocksRaycasts = true;

        foreach (Transform t in menuContainer)
            t.gameObject.SetActive(false);

        if (currentMenuIndex == index)
        {
            currentMenuIndex = -1;
            return;
        }

        menuContainer.GetChild(index).gameObject.SetActive(true);
        currentMenuIndex = index;
    }
    public void CloseMenu()
    {
        menuGroup.alpha = 0;
        menuGroup.interactable = false;
        menuGroup.blocksRaycasts = false;
    }
    #endregion

    #region notification
    public void NotifyActionButton(ActionBarButton btn)
    {
        Log.WriteLine(LogType.Information, "Notification sent for " + btn.ToString());
        actionButtons[(int)btn].transform.GetChild(0).gameObject.SetActive(true);
    }
    public void RemoveNotification(ActionBarButton btn)
    {
        RemoveNotification((int)btn);
    }
    public void RemoveNotification(int index)
    {
        actionButtons[index].transform.GetChild(0).gameObject.SetActive(false);
    }
    #endregion
}