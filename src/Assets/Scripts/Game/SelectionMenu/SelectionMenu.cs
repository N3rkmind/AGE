﻿using UnityEngine;
using System.Collections;

public class SelectionMenu : MonoSingleton<SelectionMenu>
{
    private CanvasGroup canvasGroup;

    public SelectionBuild selectionBuild;

    #region Specific Menus
    public void ShowBuilding(Building b)
    {
        selectionBuild.Show(b);
    }
    #endregion
}