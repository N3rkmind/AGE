﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectionBuild : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    private Building target;
    private bool active;
    private Job activeJob;

    public Button sellButton;
    public Button closeButton;
    public Button completeButton;
    public Button upgradeButton;

    public Text buildingName;
    public Transform progressBar;

    private Text progressText;

    private float updateRate = 1.0f;
    private float lastUpdate;

    private void Start()
    {
        progressText = progressBar.transform.parent.GetComponentInChildren<Text>();
        canvasGroup = GetComponent<CanvasGroup>();
        Hide();
    }
    private void Update()
    {
        if (active)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Hide();

            if (activeJob != null)
            {
                // If there is a current job
                if (Time.time - lastUpdate > updateRate)
                {
                    lastUpdate = Time.time;
                    completeButton.interactable = JobManager.IsJobCompleted(activeJob);
                    UpdateProgressBar();
                }
            }
        }
    }
    public void Show(Building b)
    {
        // Is there a job assigned?
        activeJob = JobManager.instance.GetJob(b);

        if (activeJob != null)
        {
            progressText.text = activeJob.jobName;

            // Is the job complete?
            completeButton.interactable = JobManager.IsJobCompleted(activeJob);

            UpdateProgressBar();
        }
        else
        {
            UpdateProgressBar();
            progressText.text = "No active job";
        }

        canvasGroup.alpha = 1;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        target = b;
        active = true;

        buildingName.text = b.Name;
    }
    public void Hide()
    {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        active = false;
        target = null;
        Grid.instance.ClearSelection();
    }

    private void UpdateProgressBar()
    {
        if (activeJob == null)
        {
            progressBar.transform.localScale = Vector3.one;
            return;
        }

        float timeIn = Helper.GetTimeStamp() - activeJob.startTime;
        float ratio = timeIn / activeJob.cost[0].duration;

        progressBar.transform.localScale = new Vector3(Mathf.Clamp(ratio,0,1), 1, 1);
    }

    #region buttons
    public void ButtonComplete()
    {
        JobManager.instance.CompleteJob(activeJob);
        Hide();
    }
    public void ButtonSell()
    {
        GameScene.instance.Grid.SellFromGrid(target);
        Hide();
    }
    public void ButtonClose()
    {
        Hide();
    }
    public void ButtonUpgrade()
    {
        WWWForm upgradeForm = new WWWForm();
        upgradeForm.AddField("key", User.ThisUser.Key);
        upgradeForm.AddField("activeId", target.ActiveId);
        upgradeForm.AddField("dataId", target.DataId);

        WWW upgradeReq = new WWW(Web.startUpgradeJob,upgradeForm);

        StartCoroutine(WaitForUpgradeReq(upgradeReq));
    }
    private IEnumerator WaitForUpgradeReq(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "SELECTIONBUILD::WaitForUpgradeReq");
            yield break;
        }

        Log.WriteLine(LogType.Information, www.text);

        switch (www.text[0])
        {
            case '0':
                string[] data = www.text.Split('|');

                int callerId = int.Parse(data[3]);

                // Replace the building
                Log.WriteLine(LogType.Good, "updated job");
                Building b = Grid.instance.GetBuildingById(callerId);
                JobManager.instance.RemoveJob(JobManager.instance.GetJob(b));

                b.RefreshVisual();

                JobManager.instance.StartUpgradeJob(Data.GetUpgradeJob(int.Parse(data[1])), int.Parse(data[3]), int.Parse(data[2]));
                Hide();
                break;

            default:
                Log.WriteLine(LogType.Error, "Unable to complete job");
                break;
        }
    }
    #endregion
}