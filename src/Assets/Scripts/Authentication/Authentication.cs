﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class Authentication : MonoSingleton<Authentication>
{
    public static string Key { set; get; }

    private WWW loginRequest;
    private WWWForm loginForm;

    public override void Init()
    {
        DontDestroyOnLoad(this.gameObject);
        /*
        emailInput = GameObject.Find("EmailInput").GetComponent<InputField>();
        passwordInput = GameObject.Find("PasswordInput").GetComponent<InputField>();

        dependencyStatus = FirebaseApp.CheckDependencies();
        if (dependencyStatus != DependencyStatus.Available)
        {
            FirebaseApp.FixDependenciesAsync().ContinueWith(task =>
            {
                dependencyStatus = FirebaseApp.CheckDependencies();
                if (dependencyStatus == DependencyStatus.Available)
                {
                    InitializeFirebase();
                }
                else
                {
                    // This should never happen if we're only using Firebase Analytics.
                    // It does not rely on any external dependencies.
                    Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
                }
            });
        }
        else
        {
            InitializeFirebase();
        }
        */
    }
    private void InitializeFirebase()
    {
        /*
        auth = FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        */
    }
    public void Disconnect()
    {
        User.ThisUser.DisconnectUser();
        Key = "";
        SceneManager.LoadScene("Auth");
    }

    #region events
    public void LoginRequest(string email, string pass)
    {
        if (email == "")
        {
            Log.WriteLine(LogType.Warning, "You must enter a email");
            return;
        }

        if (pass == "")
        {
            Log.WriteLine(LogType.Warning, "You must enter a Password");
            return;
        }

        loginForm = new WWWForm();
        loginForm.AddField("email", email);

        // Hash password
        Dictionary<string, string> headers = loginForm.headers;
        byte[] rawData = loginForm.data;
        headers["Auth"] = Helper.Sha256String(pass);

        loginRequest = new WWW(Web.userLoginEmail, rawData, headers);
        StartCoroutine(WaitSignInRes(loginRequest));
    }
    private IEnumerator WaitSignInRes(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Unable to reach server");
            yield break;
        }

        string[] data = www.text.Split('|');
        Log.WriteLine(LogType.Information,"Login response : " + Helper.RemoveSpecialCharacters(www.text));
        switch (data[0])
        {
            case "cnn":
                Log.WriteLine(LogType.Good, "Signed in with : " + data[1]);
                User.instance.SetUser(data[2],int.Parse(data[3]), data[4]);
                SceneManager.LoadScene("Menu");
                break;

            case "1":
                Log.WriteLine(LogType.Error, "Name is empty!");
                break;

            case "2":
                Log.WriteLine(LogType.Error, "Password is empty!");
                break;

            case "3":
                Log.WriteLine(LogType.Error, "Username does not exist");
                break;

            case "4":
                Log.WriteLine(LogType.Error, "Wrong password");
                break;

            default:
                Log.WriteLine(LogType.Error, "Server Error : Unable to reach the server, try again later");
                break;
        }
    }
    public void OnLoginButton()
    {

    //    SigninWithCredential(emailInput.text, passwordInput.text);
    }
    /*
    public void OnCreateButton()
    {
        if (emailInput.text == "")
        {
            Debug.Log("You must enter a email");
            return;
        }

        if (passwordInput.text == "")
        {
            Debug.Log("You must enter a Password");
            return;
        }

        CreateUser(emailInput.text, passwordInput.text);
    }
    public void OnGoogleButton()
    {
        Debug.Log("Not implemented");
    }
    public void OnFacebookButton()
    {
        Debug.Log("Not implemented");
    }
    private void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        
        if (auth.CurrentUser != user)
        {
            if (user == null && auth.CurrentUser != null)
            {
                Debug.Log("Signed in " + auth.CurrentUser.DisplayName);
            }
            else if (user != null && auth.CurrentUser == null)
            {
                Debug.Log("Signed out " + user.DisplayName);
            }
            user = auth.CurrentUser;
        }
        
    }
    */
    #endregion

    #region create
    /*
    public void CreateUser(string email,string password)
    {
        Debug.Log(string.Format("Attempting to create user {0}...", email));
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(HandleCreateResult);
    }
    private void HandleCreateResult(Task<FirebaseUser> authTask)
    {
        if (authTask.IsCanceled)
        {
            Debug.Log("User Creation canceled.");
        }
        else if (authTask.IsFaulted)
        {
            Debug.Log("User Creation encountered an error.");
            Debug.Log(authTask.Exception.ToString());
        }
        else if (authTask.IsCompleted)
        {
            Debug.Log("User Creation completed.");
            if (auth.CurrentUser != null)
            {
                Debug.Log("User Info: " + auth.CurrentUser.Email + "   " + auth.CurrentUser.ProviderId);
            }
            Debug.Log("Signing out.");
            auth.SignOut();
        }
    }
    */
    #endregion

    #region signin
    /*
    public void Signin(string email,string password)
    {
        Debug.Log(string.Format("Attempting to sign in as {0}...", email));
        auth.SignInWithEmailAndPasswordAsync(email, password)
          .ContinueWith(HandleSigninResult);
    }
    public void SigninWithCredential(string email,string password)
    {
        Debug.Log(String.Format("Attempting to sign in as {0}...", email));
        Credential cred = EmailAuthProvider.GetCredential(email, password);
        auth.SignInWithCredentialAsync(cred).ContinueWith(HandleSigninResult);
    }
    public void SigninAnonymously()
    {
        Debug.Log("Attempting to sign anonymously...");
        auth.SignInAnonymouslyAsync().ContinueWith(HandleSigninResult);
    }
    private void HandleSigninResult(Task<FirebaseUser> authTask)
    {
        if (authTask.IsCanceled)
        {
            Debug.Log("SignIn canceled.");
        }
        else if (authTask.IsFaulted)
        {
            Debug.Log("Login encountered an error.");
            Debug.Log(authTask.Exception.ToString());
        }
        else if (authTask.IsCompleted)
        {
            Debug.Log("Login completed.");
            Debug.Log("Signing out.");
            auth.SignOut();
        }
    }
    */
    #endregion
}