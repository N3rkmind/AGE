﻿using UnityEngine;
using System.Collections;

public class User : MonoSingleton<User>
{
    public static User ThisUser { get { return thisUser; } }
    private static User thisUser;

    private string username;
    private int userId;
    private string accessToken;

    // For menu loading
    private bool resourceReady = false;
    private bool guildReady = false;

    #region Init
    public override void Init()
    {
        thisUser = null;
    }
    #endregion

    #region Set&Get
    public void SetUser(string username,int userId,string accessToken)
    {
        this.username = username;
        this.userId = userId;
        this.accessToken = accessToken;
        thisUser = this;
    }
    public void DisconnectUser()
    {
        thisUser = null;
        username = "";
        accessToken = "";

    }
    public string Username { get { return username; } }
    public string Key { get { return accessToken; } }
    public int UserId { get { return userId; } }
    #endregion


    private void CheckIfGridReady()
    {
        if (resourceReady /*&& guildReady*/)
        {
            MenuScene.instance.SetGridReady();
        }
    }
}