﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Web : MonoSingleton<Web>
{
    #region urls
    public static string host = "http://127.0.0.1:80/n3age";
  //  public static string host = "http://23.92.25.119/n3age";
    public static string user = host + "/user";
    public static string grid = host + "/grid";
    public static string job = host + "/job";
    public static string create = host + "/create";
    public static string resource = host + "/resource";
    public static string reward = host + "/reward";
    public static string cost = host + "/cost";

    public static string userLoginEmail = user + "/loginEmail.php";
    public static string userLoginGoogle = user + "/loginGoogle.php";
    public static string userLoginFacebook = user + "/loginFacebook.php";

    public static string gridLoad = grid + "/loadGrid.php";
    public static string gridAdd = grid + "/addToGrid.php";
    public static string gridSell = grid + "/sellFromGrid.php";
    public static string gridFetchBuilding = grid + "/fetchBuildingList.php";

    public static string getJobList = job + "/getJobList.php";
    public static string getActiveJobs = job + "/getActiveJob.php";
    public static string completeBuild = job + "/completeBuild.php";
    public static string completeGather = job + "/completeGather.php";
    public static string completeUpgrade = job + "/completeUpgrade.php";
    public static string startUpgradeJob = job + "/startUpgradeJob.php";

    public static string createCheckEmail = create + "/checkEmail.php";
    public static string createUser = create + "/createUser.php";

    public static string resourceFetchList = resource + "/getResourceList.php";
    public static string resourceGet = resource + "/getResource.php";

    public static string getRewardList = reward + "/getRewardList.php";

    public static string getCostList = cost + "/getCostList.php";
#endregion

    public override void Init()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void OnWebMessage(string msg)
    {
        string[] op = msg.Split('|');

        Debug.Log("Web msg : " + msg);

        switch (op[0])
        {
            case "cnn":
                // cnn | platform | name | token 
                if (op.Length < 4)
                {
                    Log.WriteLine(LogType.Error,"cnn missing args");
                    return;
                }

                if (op[1] == "email")
                {
                    Log.WriteLine(LogType.Good, "Email Signed in as " + op[2]);
                    Log.WriteLine(LogType.Warning, "Guild not implemented, stub MGN");
                    User.instance.SetUser(op[2], int.Parse(op[3]),op[4]);
                    SceneManager.LoadScene("Menu");
                }

                if (op[1] == "google")
                {
                    Log.WriteLine(LogType.Good, "Google Signed in as " + op[2]);
                    Log.WriteLine(LogType.Warning, "Guild not implemented, stub MGN");
                    User.instance.SetUser(op[2] ,int.Parse(op[3]),op[4]);
                    SceneManager.LoadScene("Menu");
                }
                break;
            case "grd":
                GameScene.instance.Grid.LoadGrid(op[1]);
                break;
            case "crt":
                User.ThisUser.SetUser(op[2], -1, op[3]);
                SceneManager.LoadScene("CreateUser");
                break;
            default:
                Log.WriteLine(LogType.Error, "request error : " + op);
                break;
        }
    }
}
