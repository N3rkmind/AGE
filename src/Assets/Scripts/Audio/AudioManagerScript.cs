﻿/// <summary>
/// Audio manager script.
/// 
/// As the name suggests, this is the Audio Manage Script. As of now, this script is resposible for handling pretty much all audio in the 
/// game. The script first downloads all specified AssetBundles form the server, and stores all of them into an AssetBundle array. Then, 
/// all assets are loaded from all the asset bundles, and the AudioClip arrays are populated. Once the AudioClip array have been pouplated,
/// all the tracks are ready for use.
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class AudioManagerScript : MonoBehaviour {

    public static AudioManagerScript Instance { set; get; }

    [Header("Audio Players")]
	public AudioSource[] audioSources; // Stores references to all AudioSource components found in child gameobjects.

	[Header("AudioClip Arrays")]
	public AudioClip[] bgMusicAudioClips; // AudioClip array for Background Music audioclips.
	public AudioClip[] fxAudioClips; // AudioClip array for SoundFX audioclips.

	UnityWebRequest www; // API to communicate with server.
	AsyncOperation ao; // Required because otherwise www.Send() doesn't work.
	AssetBundle[] audioBundles; // AssetBundles array.
	private bool shouldLoadAllAssets = false; // Should all the assets be loaded?
	public bool bgMusicLoaded = false;
	public bool audioFXLoaded = false;

	System.Random random;
	private int randomNumber = 0;
	private int prevRandomNumber = 0;

	// Use this for initialization
	void Start () 
	{
        Instance = this;
        audioSources = transform.GetComponentsInChildren<AudioSource>(); // Find all AudioSources in children.
		random = new System.Random();
		DontDestroyOnLoad(this.gameObject); // Make sure this GameObject doesn't get destroyed when another scene loads.
		StartCoroutine(DownloadAssetBundles()); // Download all asset bundles and populate audioBundles array.
	}

	// Update is called once per frame
	void Update ()
	{
		if(shouldLoadAllAssets) // If flag is true...
		{
			// Load assets from bundles and store them into AudioClip arrays.
			bgMusicAudioClips = audioBundles[0].LoadAllAssets<AudioClip>();
			bgMusicLoaded = true;
			fxAudioClips = audioBundles[1].LoadAllAssets<AudioClip>();
			audioFXLoaded = true;

			shouldLoadAllAssets = false; // Set flag to false so assets to load in the next Update.
		}
	}

	void OnApplicationQuit()
	{
		for(int i = 0; i < audioBundles.Length; i++)
		{
			audioBundles[i].Unload(true);
		}
	}

	// This method downloads all the AssetBundles and populates the audioBundles array with them.
	IEnumerator DownloadAssetBundles()
	{
		// URLs for all the AssetBundles being downloaded.
		string[] urlArr = new string[2];
		urlArr[0] = Web.host + "/AssetBundles/audiotracks/background";
		urlArr[1] = Web.host + "/AssetBundles/audiotracks/fx";
//		urlArr[0] = "http://192.168.140.128/n3age/AssetBundlesStandalone/audiotracks/background";
//		urlArr[1] = "http://192.168.140.128/n3age/AssetBundlesStandalone/audiotracks/fx";

		audioBundles = new AssetBundle[urlArr.Length]; // Initialize audioBundles array. Size should be same as urlArr length.

		for(int i = 0; i < urlArr.Length; i++)
		{
			www = UnityWebRequest.GetAssetBundle(urlArr[i]); // Object to handle server communication.
			Log.WriteLine("Trying to connect to server");
			Debug.Log("Trying to connect to server");
			ao = www.Send(); // Communicate with server.

			while(!www.isDone) // While download is not complete...
			{
				Log.WriteLine("Connected. Downloading AssetBundle.");
				Debug.Log("Connected. Downloading AssetBundle.");
				yield return null; //...return back and continue next frame.
			}

			audioBundles[i] = DownloadHandlerAssetBundle.GetContent(www); // Store AssetBundle into audioBundles array item.
			Debug.Log("Asset Downloaded.");

			www.Dispose(); // Dispose of the Object to conserve memory.
			Log.WriteLine("AssetBundle " + audioBundles[i].name + " downloaded.");
		}

		shouldLoadAllAssets = true; // Set flag to true so at next Update, assets get loaded.
		StopCoroutine(DownloadAssetBundles()); // Stop Coroutine so it doesn't continue running in background.
	}


	/////////////////////////////////////////////////////
	// Methods to be called from outside the script.
	/////////////////////////////////////////////////////

	// Plays all the music in the bgMusicAudioClips array on a loop in a random order.
	public void UpdateBackgroundMusicPlayer()
	{
		int audSourceIndex = (int)AudioPlayerName.BackgroundMusicPlayer;
		AudioSource audSource = audioSources[audSourceIndex];

		randomNumber = random.Next(0, bgMusicAudioClips.Length - 1);
		// Just to make sure the next random number isn't the same as the previos one.
		if(randomNumber == prevRandomNumber) randomNumber = random.Next(0, bgMusicAudioClips.Length - 1);

		if(!audSource.isPlaying)
		{
			audSource.clip = bgMusicAudioClips[random.Next(0, bgMusicAudioClips.Length-1)];
			audSource.Play();
		}
	}

	/// Looks for clipName. If found, the clip is played via playerName AudioSource.
	public void PlaySound(string clipName, AudioPlayerName playerName)
	{
		AudioClip clip;
		AudioSource audSource = audioSources[(int)playerName];

		// Check if AudioClip exists or not.
		for(int i = 0; i < fxAudioClips.Length; i++)
		{
			if(fxAudioClips[i].name == clipName)
			{
				clip = fxAudioClips[i];
				audSource.clip = clip;
				audSource.Play();
				return;
			}
		}

		// If the execution made it this far, we have a problem...
		Log.WriteLine("Clip not found. Check to make sure clip exist and that you have entered the correct name!");
	}
}

// Number of AudioSource GameObjects must ALWAYS be equal to number of items in this enum. ALWAYS!
public enum AudioPlayerName
{
	BackgroundMusicPlayer = 0, 
	UIAudioFXPlayer = 1, 
	GeneralAudioFXPlayer = 2,
	JobStartAudioFXPlayer = 3,
	JobFinishAudioFXPlayer = 4,
};
