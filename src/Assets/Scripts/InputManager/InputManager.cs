﻿using UnityEngine;
using System.Collections;

public static class InputManager
{ 
	// -- Axis

	public static float MainHorizontal()
	{
		float r = 0.0f;
		r += Input.GetAxis ("J_MainHorizontal");
		r += Input.GetAxis ("K_MainHorizontal");
		return Mathf.Clamp (r, -1.0f, 1.0f);
	}

	public static float MainVertical()
	{
		float r = 0.0f;
		r += Input.GetAxis ("J_MainVertical");
		r += Input.GetAxis ("K_MainVertical");
		return Mathf.Clamp (r, -1.0f, 1.0f);
	}

	public static Vector3 MainJoystick()
	{
		return new Vector3 (MainHorizontal (), 0, MainVertical ());
	}

	public static float SecondaryHorizontal()
	{
		float r = 0.0f;

        if (Input.GetMouseButton(1))
            r += Input.GetAxis("Mouse X");

		r += Input.GetAxis ("J_SecondaryHorizontal");
		r += Input.GetAxis ("K_SecondaryHorizontal");

        return Mathf.Clamp (r, -1.0f, 1.0f);
	}

	public static float SecondaryVertical()
	{
		float r = 0.0f;

        if (Input.GetMouseButton(1))
            r += Input.GetAxis("Mouse Y");

        r += Input.GetAxis ("J_SecondaryVertical");
		r += Input.GetAxis ("K_SecondaryVertical");

		return Mathf.Clamp (r, -1.0f, 1.0f);
	}

	public static Vector3 SecondaryJoystick()
	{
		return new Vector3 (SecondaryHorizontal (), 0, SecondaryVertical ());
	}

	// -- Buttons

	public static bool AButton()
	{
		return Input.GetButtonDown ("A_Button");
	}

	public static bool BButton()
	{
		return Input.GetButtonDown ("B_Button");
	}

	public static bool XButton()
	{
		return Input.GetButtonDown ("X_Button");
	}

	public static bool YButton()
	{
		return Input.GetButtonDown ("Y_Button");
	}

	public static bool StartButton()
	{
		return Input.GetButtonDown ("Start_Button");
	}

	public static bool BackButton()
	{
		return Input.GetButtonDown ("Back_Button");
	}
}