﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class Console : MonoBehaviour, ILog
{
    private CanvasGroup thisCanvas;
    private InputField cmd;
    private int index = 0;
    private Transform container;
    private int keepAmn; // Amount of logs to keep, based on the amn of objects in the container
    private bool showing;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        cmd = GetComponentInChildren<InputField>();
        thisCanvas = GetComponent<CanvasGroup>();
        container = GameObject.Find("LogContainer").transform;
        keepAmn = container.childCount;

        Log.AddReader(this);
        Log.WriteLine(LogType.Normal,"Console loaded!");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F9))
            ToggleConsole();

        if (showing)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                ParseCommand(cmd.text);
                cmd.text = "";
                cmd.Select();
            }
        }
    }

    private void ParseCommand(string cmd)
    {
        Log.WriteLine(LogType.Normal, cmd);
        string[] c = cmd.Split(' ');

        if (c.Length < 1)
        {
            Log.WriteLine(LogType.Error, "-");
            return;
        }
        switch (c[0])
        {
            #region log
            case "log":
                if (c.Length < 2)
                {
                    Log.WriteLine(LogType.Warning, "log ?");
                    break;
                }

                switch (c[1])
                {
                    case "admin":
                        FindObjectOfType<Authentication>().LoginRequest("mike.doyon@freedom.tm", "!@$$free");
                        break;
                    default:
                        Log.WriteLine(LogType.Error, "log ???");
                        break;
                }
            break;
            #endregion

            #region notify
            case "notify":
                if (c.Length < 2)
                {
                    Log.WriteLine(LogType.Warning, "notify actionbar");
                    break;
                }
                switch (c[1])
                {
                    case "actionbar":
                        if (c.Length < 3)
                        {
                            Log.WriteLine(LogType.Warning, "notify actionbar index");
                            break;
                        }
                        int index = -1;
                        int.TryParse(c[2], out index);
                        if (index != -1)
                        {
                            var a = FindObjectOfType<ActionBar>();
                            if (a != null)
                                a.NotifyActionButton((ActionBarButton)index);
                            else
                                Log.WriteLine(LogType.Error, "Action bar not found");
                        }
                        else
                            Log.WriteLine(LogType.Error, "NaN :" + c[2]);
                        break;
                }
            break;
            #endregion



            default:
                Log.WriteLine(LogType.Error, "Unable to find command");
                break;
        }

    }

    private void ToggleConsole(bool force = false)
    {
        showing = !showing;

        thisCanvas.alpha = (showing)?1:0;
        thisCanvas.blocksRaycasts = showing;
        thisCanvas.interactable = showing;
        if (showing)
            cmd.Select();
    }

    public void WriteLine(LogEntry entry)
    {
        Color logColor;
        switch (entry.Type)
        {
            case LogType.Error:
                logColor = Color.red;
                break;
            case LogType.Warning:
                logColor = Color.magenta;
                break;
            case LogType.Information:
                logColor = Color.yellow;
                break;
            case LogType.Good:
                logColor = Color.green;
                break;
            case LogType.Normal:
            default:
                logColor = Color.white;
                break;
        }

        Transform target = container.GetChild(0);
        Text t = target.GetComponent<Text>();
        t.text = "[" + Time.time.ToString("0") + "]" + " : " + entry.Message;
        t.color = logColor;
        target.SetAsLastSibling();

        index++;
        if (index == keepAmn)
            index = 0;
    }
}
