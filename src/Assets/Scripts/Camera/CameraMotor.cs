﻿using UnityEngine;
using System.Collections;

public class CameraMotor : MonoBehaviour
{
	// Inspector fields
	public bool initializeOnStart = false;
	[SerializeField]
	private Transform lookAtTransform;

	// Properties
	public Vector3 InputVector{set;get;}

	// Class fields
	private Camera cam;
	private BaseCameraState state;
	private Transform thisTransform;

	// Transition fields

	#region Start
	private void Start()
	{
		if(initializeOnStart)
			Init();
	}
	public void Init()
	{
		cam = GetComponent<Camera> ();
		thisTransform = transform;

		state = GetComponent<N3CThirdPerson> () as BaseCameraState;
		state.Construct ();
	}
	#endregion

	#region Update
	private void Update()
	{
		InputVector = PoolInput ();
	}
	private void LateUpdate()
	{
		thisTransform.position = state.ProcessMotion(InputVector);
		thisTransform.rotation = state.ProcessRotation(InputVector);
	}
	#endregion

	#region Methods
	private Vector3 PoolInput()
	{
		Vector3 dir = Vector3.zero;

		dir.x = InputManager.SecondaryHorizontal ();
		dir.y += (InputManager.YButton ()) ? 1 : 0;
		dir.y += (InputManager.XButton ()) ? -1 : 0;
		dir.z = InputManager.SecondaryVertical ();

		if (dir.sqrMagnitude > 1)
			dir.Normalize ();

		return dir;
	}
	public void ChangeState(string stateName)
	{
		state.Destruct ();
		state = GetComponent (stateName) as BaseCameraState;
		state.Construct ();
	}
	#endregion

	#region Getters
	public Transform LookAtTarget{ get { return lookAtTransform; } }
	public Transform CameraContainerTransform{ get { return thisTransform; } }
	public Camera Camera{get{ return cam;}}
	#endregion
}