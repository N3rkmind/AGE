﻿using UnityEngine;
using System.Collections;

public class BaseCameraState : MonoBehaviour
{
	protected float startTime;
	protected float immuneTime;
	protected CameraMotor motor;

	public virtual void Construct()
	{
		motor = GetComponent<CameraMotor> ();
	}

	public virtual void Destruct()
	{
		
	}

	public virtual void Transition()
	{
		
	}

	public virtual Vector3 ProcessMotion(Vector3 input)
	{
        Log.WriteLine(LogType.Error, "ProcessMotion is not implemented in " + this.ToString());
		return input;
	}

	public virtual Quaternion ProcessRotation(Vector3 input)
	{
		return Quaternion.identity;
	}
}	
