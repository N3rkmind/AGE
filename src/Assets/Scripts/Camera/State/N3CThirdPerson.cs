﻿     using UnityEngine;
using System.Collections;

public class N3CThirdPerson : BaseCameraState
{
	private const int MAX_OCCLUSION_CHECKS = 10;
	private const float OCCLUSION_DISTANCE_STEP = 0.5f;

	// Inspector fields
	public Vector3 offset = new Vector3(0,1,0);
	public float startDistance = 10;
	public float distanceMin = 2.5f;
	public float distanceMax = 15.0f;
	public float zoomSensitivity = 1.0f;
	public float verticalAngleMin = -10f;
	public float verticalAngleMax = 50f;
	public float sensitivityX = 3.0f;
	public float sensitivityY = 1.0f;
	public float xSmooth = 0.01f;
	public float ySmooth = 0.05f;
      
	private float distance;
	private float distanceVelocity;
	private float distanceSmooth;
	private float distSmooth;
	private float distanceResumeSmooth = 1.0f;
	private float distancePreOccluded;
	private float velocityX = 0f;
	private float velocityY = 0f;

	private float distanceDesired = 0.0f;
	private Vector3 desiredPosition;

	// Class fields
	private Transform lookAt;
	private Camera cam;
	private Transform camTransform;
	private float currentX;
	private float currentY;

	#region State Implementation
	public override void Construct ()
	{
		base.Construct ();
		lookAt = motor.LookAtTarget;
		camTransform = motor.CameraContainerTransform;
		cam = motor.Camera;
		distance = startDistance;
		distanceDesired = distance;
		distancePreOccluded = distance;
	}
	public override Vector3 ProcessMotion (Vector3 input)
	{
		if (lookAt == null)
			return Vector3.zero;

		currentX += input.x * sensitivityX;
		currentY -= input.z * sensitivityY;
		currentY = CameraHelper.ClampAngle (currentY, verticalAngleMin, verticalAngleMax);

		if (input.y != 0)
		{
			distanceDesired = Mathf.Clamp (distance - input.y * zoomSensitivity, distanceMin, distanceMax);
			distancePreOccluded = distanceDesired;
			distSmooth = distanceSmooth;
		}

		int count = 0;
		do {
			CalculatePosition();
			count++;
		} while(CheckIfOccluded (count));

		// Calculate position;
		Vector3 playerPos = lookAt.position + offset;
		float posX = Mathf.SmoothDamp(transform.position.x, desiredPosition.x, ref velocityX, xSmooth);
		float posY = Mathf.SmoothDamp(transform.position.y, desiredPosition.y, ref velocityY, ySmooth);
		float posZ = Mathf.SmoothDamp(transform.position.z, desiredPosition.z, ref velocityX, xSmooth);

		return new Vector3(posX,posY,posZ);
	}
	public override Quaternion ProcessRotation (Vector3 input)
	{
		camTransform.LookAt (lookAt.position + offset);
		return camTransform.rotation;
	}
	#endregion

	#region Methods
	private void CalculatePosition()
	{
		ResetDesiredDistance();
		distance = Mathf.SmoothDamp(distance, distanceDesired, ref distanceVelocity, distSmooth);
		desiredPosition = CalculatePosition(currentY, currentX, distance);
	}
	private Vector3 CalculatePosition(float rotX,float rotY,float distance)
	{
		Vector3 direction = new Vector3(0,0,-distance);
		Quaternion rotation = Quaternion.Euler(rotX,rotY,0);
		return (lookAt.position + offset)+ rotation * direction;
	}
	private bool CheckIfOccluded(int _count)
	{
		bool isOccluded = false;
		float nearestDistance = CheckCameraPoints(lookAt.position, desiredPosition);

		if(nearestDistance != -1)
		{
			if(_count < MAX_OCCLUSION_CHECKS)
			{
				isOccluded = true;
				distance -= OCCLUSION_DISTANCE_STEP;

				if (distance < 0.25f)
					distance = 0.25f;
			}
			else
			{
				distance = nearestDistance - cam.nearClipPlane;
			}                      

			distanceDesired = distance;
			distSmooth = distanceResumeSmooth;
		}
			                              
		return isOccluded;
	}
	private float CheckCameraPoints(Vector3 _from,Vector3 _to)
	{
		float nearestDistance = -1f;

		RaycastHit hit;

		CameraBounds clipPlanePoint = ClipPlaneAtNear(_to);

#if UNITY_EDITOR
//		Debug.DrawLine(_from, _to + camTransform.forward * -cam.nearClipPlane, Color.red);
//		Debug.DrawLine(_from, clipPlanePoint.UpperLeft);
//		Debug.DrawLine(_from, clipPlanePoint.UpperRight);
//		Debug.DrawLine(_from, clipPlanePoint.LowerLeft);
//		Debug.DrawLine(_from, clipPlanePoint.LowerRight);
//
//		Debug.DrawLine(clipPlanePoint.UpperLeft, clipPlanePoint.UpperRight);
//		Debug.DrawLine(clipPlanePoint.UpperRight, clipPlanePoint.LowerRight);
//		Debug.DrawLine(clipPlanePoint.LowerRight, clipPlanePoint.LowerLeft);
//		Debug.DrawLine(clipPlanePoint.LowerLeft, clipPlanePoint.UpperLeft);
#endif

		if(Physics.Linecast(_from , clipPlanePoint.UpperLeft, out hit) && hit.collider.tag != "Player")
			nearestDistance = hit.distance;

		if(Physics.Linecast(_from , clipPlanePoint.LowerLeft, out hit) && hit.collider.tag != "Player")
			if (hit.distance < nearestDistance || nearestDistance == -1)
				nearestDistance = hit.distance;

		if(Physics.Linecast(_from , clipPlanePoint.UpperRight, out hit) && hit.collider.tag != "Player")
			if (hit.distance < nearestDistance || nearestDistance == -1)
				nearestDistance = hit.distance;

		if(Physics.Linecast(_from , clipPlanePoint.LowerRight, out hit) && hit.collider.tag != "Player")
			if (hit.distance < nearestDistance || nearestDistance == -1)
				nearestDistance = hit.distance;

		if(Physics.Linecast(_from , _to + transform.forward * -cam.nearClipPlane, out hit) && hit.collider.tag != "Player")
		if (hit.distance < nearestDistance || nearestDistance == -1)
			nearestDistance = hit.distance;

		return nearestDistance;
	}
	private void ResetDesiredDistance()
	{
		if(distanceDesired < distancePreOccluded)
		{
			Vector3 pos = CalculatePosition(currentY,currentX,distancePreOccluded);
			float nearestDistance = CheckCameraPoints(lookAt.position,pos);

			if(nearestDistance == -1 || nearestDistance > distancePreOccluded)
			{
				distanceDesired = distancePreOccluded;
			}
		}
	}
	private struct CameraBounds
	{
		public Vector3 UpperLeft;
		public Vector3 UpperRight;
		public Vector3 LowerLeft;
		public Vector3 LowerRight;
	};
	private CameraBounds ClipPlaneAtNear(Vector3 _pos)
	{
		var bounds = new CameraBounds();

		if(cam == null)
			return bounds;

		var transform = cam.transform;
		var halfFOV = (cam.fieldOfView / 2) * Mathf.Deg2Rad;
		var aspect = cam.aspect;
		var distance = cam.nearClipPlane;

		var height = distance * Mathf.Tan(halfFOV);
		var width = height * aspect;

		bounds.LowerRight = _pos + transform.right * width;
		bounds.LowerRight -= transform.up * height;
		bounds.LowerRight += transform.forward * distance;

		bounds.LowerLeft = _pos - transform.right * width;
		bounds.LowerLeft -= transform.up * height;
		bounds.LowerLeft += transform.forward * distance;

		bounds.UpperRight = _pos + transform.right * width;
		bounds.UpperRight += transform.up * height;
		bounds.UpperRight += transform.forward * distance;

		bounds.UpperLeft = _pos - transform.right * width;
		bounds.UpperLeft += transform.up * height;
		bounds.UpperLeft += transform.forward * distance;

		return bounds;
	}
	#endregion
}