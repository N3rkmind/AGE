﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum JobType
{
    build,
    gather,
    upgrade,
}

public class Job
{
    // From the Database
    public Job(
        int dataId,
        string name,
        string type,
        int callerDataId,
        string callerTable,
        string targetName,
        int rewardId)
    {
        this.activeId = 0;
        this.dataId = dataId;
        this.jobName = name;
        this.type = Data.GetJobTypeByName(type);
        this.callerDataId = callerDataId;
        this.callerTable = Data.GetTargetTable(callerTable);
        this.targetTable = Data.GetTargetTable(targetName);
        this.reward = Data.GetReward(rewardId);
        cost = new List<Cost>();
    }

    // From Loadup
    public Job(
        int activeId,
        int dataId,
        string name,
        string type,
        int callerDataId,
        string callerName,
        int callerId,
        string targetName,
        int targetId,
        int rewardId,
        int startTime)
    {
        this.activeId = activeId;
        this.dataId = dataId;
        this.jobName = name;
        this.type = Data.GetJobTypeByName(type);
        this.callerDataId = callerDataId;
        this.callerTable = Data.GetTargetTable(callerName);
        this.callerId = callerId;
        this.targetTable = Data.GetTargetTable(targetName);
        this.targetId = targetId;
        this.reward = Data.GetReward(rewardId);
        this.startTime = startTime;
        cost = Data.GetCostForJob(dataId);
    }

    public Job(Job j, int activeId, int callerId,int targetId,int startTime)
    {
        this.activeId = activeId;
        this.dataId = j.dataId;
        this.jobName = j.jobName;
        this.type = j.type;
        this.callerDataId = j.callerDataId;
        this.callerTable = j.callerTable;
        this.cost = j.cost;
        this.reward = j.reward;

        this.callerId = callerId;
        this.targetId = targetId;
        this.startTime = startTime;
    }

    public int activeId;
    public int dataId;
    public string jobName;
    public int startTime;
    public int callerDataId;
    public int callerId;
    public int targetId;
    public bool completed;
    public Reward reward;
    public List<Cost> cost;
    public JobType type;
    public TargetTable callerTable;
    public TargetTable targetTable;
}