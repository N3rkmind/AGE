﻿using UnityEngine;
using System.Collections;

public class Reward
{
    public Reward(int id, string table, int targetId, string param)
    {
        this.rewardId = id;
        this.table = Data.GetTargetTable(table);
        this.targetId = targetId;
        this.param = param;
    }

    public int rewardId;
    public TargetTable table;
    public int targetId;
    public string param;
}
