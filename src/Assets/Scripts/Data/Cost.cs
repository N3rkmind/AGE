﻿using UnityEngine;
using System.Collections;

public class Cost
{
    public Cost(int id,int jobId,int resourceId,int resourceAmn,int duration)
    {
        this.costId = id;
        this.jobId = jobId;
        this.resourceType = Data.GetResourceType(resourceId);
        this.resourceAmn = resourceAmn;
        this.duration = duration;
    }

    public int costId;
    public int jobId;
    public ResourceType resourceType;
    public int resourceAmn;
    public int duration;
}
