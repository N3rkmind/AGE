﻿using System;
using UnityEngine;

public enum ResourceType
{
    Wood = 0,
    Food = 1,
    Stone = 2,
    Gold = 3,
}

public class Resource
{
    public Resource(int id, string name)
    {
        resourceId = id;

        switch (name)
        {
            case "Wood":
                type = ResourceType.Wood;
                break;
            case "Food":
                type = ResourceType.Food;
                break;
            case "Stone":
                type = ResourceType.Stone;
                break;
            case "Gold":
                type = ResourceType.Gold;
                break;
            default:
                Log.WriteLine(LogType.Error, "Wrong resource");
                break;
        }
    }

    public int resourceId;
    public ResourceType type;
    public int amn;
}