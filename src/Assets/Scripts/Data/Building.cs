﻿using UnityEngine;
using System.Collections;

public class Building
{
    private int dataId;
    private string buildingName;
    private int sizeX;
    private int sizeY;
    private int modelId;
    private int level;

    private int posX;
    private int posY;
    private int activeId;
    private GameObject go;

    // For Data purpose
    public Building(int dataId, string name,int sizeX,int sizeY,int modelId,int level)
    {
        this.dataId = dataId;
        this.buildingName = name;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.modelId = modelId;
        this.level = level;
    }
    // For in-game spawning
    public Building(int dataId, int activeId, int sizeX, int sizeY, int modelId, int level,int posX,int posY)
    {
        this.dataId = dataId;
        this.activeId = activeId;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.modelId = modelId;
        this.level = level;
        this.posX = posX;
        this.posY = posY;
    }
    // Copy
    public Building(Building b)
    {
        this.go = b.Go;
        this.buildingName = b.buildingName;
        this.dataId = b.dataId;
        this.activeId = b.ActiveId;
        this.sizeX = b.SizeX;
        this.sizeY = b.SizeY;
        this.modelId = b.ModelId;
        this.level = b.Level;
        this.posX = b.PosX;
        this.posY = b.PosY;
    }

    public GameObject CreateVisual(bool constructing = false)
    {
        GameObject visual;
        Job j = JobManager.instance.GetJob(this);
        if (j != null && j.type == JobType.build)
        {
            visual = Resources.Load("Building/Model/0") as GameObject;
            visual.transform.localScale = new Vector3(SizeX, 1, sizeY);
        }
        else
            visual = Resources.Load("Building/Model/" + modelId.ToString()) as GameObject;

        if(visual == null)
        {
            visual = Resources.Load("Building/Model/0") as GameObject;
            Log.WriteLine(LogType.Warning, "No visual found for : " + modelId.ToString() + " , using default");
        }

        go = MonoBehaviour.Instantiate(visual) as GameObject;
        return go;
    }

    public void RefreshVisual()
    {
        Vector3 pos = Go.transform.position;
        UnityEngine.MonoBehaviour.Destroy(Go);
        modelId = dataId;
        CreateVisual();
        go.transform.position = pos;
    }

    public void UpgradeBuilding(Building newBuilding)
    {
        dataId = newBuilding.dataId;
        sizeX = newBuilding.sizeX;
        sizeY = newBuilding.sizeY;
        buildingName = newBuilding.buildingName;
        level = newBuilding.level;
        modelId = newBuilding.modelId;

        RefreshVisual();
    }

    #region set&get
    public GameObject Go { set { go = value; } get { return go; } }
    public int DataId { set { dataId = value; }  get { return dataId; } }
    public int ActiveId { set { activeId = value; } get { return activeId; } }
    public int PosX { get { return posX; } }
    public int PosY { get { return posY; } }
    public int SizeX { get { return sizeX; } }
    public int SizeY { get { return sizeY; } }
    public string Name { get { return buildingName; } }
    public int Level { get { return level; } }
    public int ModelId { get { return modelId; } }
    public void SetGridPosition(int x, int y)
    {
        posX = x;
        posY = y;
    }
    #endregion

    public override string ToString()
    {
        return string.Format("{0} activeId:{1} | buildingId:{2}, PosX:{3}, PosY:{4}, Size:{5}:{6}, Level:{7}", Name, ActiveId, DataId, PosX, PosY, sizeX, sizeY, level);
    }
}
