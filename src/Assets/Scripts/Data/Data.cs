﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using System;

public enum TargetTable
{
    Resource,
    Building,
    Cost,
    Reward,
    Job
}

public class Data : MonoSingleton<Data>
{
    #region static data
    public static List<Building> buildings;
    public static List<Resource> resources;
    public static List<Cost> costs;
    public static List<Reward> rewards;
    public static List<Job> jobs;

    private bool buildingReady = false;
    private bool resourceReady = false;
    private bool jobReady = false;
    private bool rewardReady = false;
    private bool costReady = false;
    #endregion

    public override void Init()
    {
        DontDestroyOnLoad(this.gameObject);

        FetchResourceList();
        FetchBuildingList();
        FetchRewardList();
        FetchJobList();
        FetchCostList();
    }

    #region buildings
    public static Building GetBuildingByName(string name)
    {
        Building r = buildings.Find(x => x.Name == name);
        if (r == null)
            Log.WriteLine(LogType.Error, "Unable to find building : " + name);

        return r;
    }
    public static Building GetBuildingById(int index)
    {
        Building r = buildings.Find(x => x.DataId == index);
        if (r == null)
            Log.WriteLine(LogType.Error, "Unable to find building : " + index);

        return r;
    }
    private void FetchBuildingList()
    {
        WWW reqBuilding = new WWW(Web.gridFetchBuilding);

        StartCoroutine(WaitForBuildingReq(reqBuilding));
    }
    private IEnumerator WaitForBuildingReq(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "FetchBuildingList::Empty Response" );
            yield break;
        }

        //JSON data
        Debug.Log(www.text);
        JsonData builds = JsonMapper.ToObject(new JsonReader(www.text));

        buildings = new List<Building>();
        for (int i = 0; i < builds.Count; i++)
        {
            int id = int.Parse((string)builds[i]["id"]);
            string name = (string)builds[i]["name"];
            int sizeX = int.Parse((string)builds[i]["size_x"]);
            int sizeY = int.Parse((string)builds[i]["size_y"]);
            int modelId = int.Parse((string)builds[i]["model_id"]);
            buildings.Add(new Building(id, name, sizeX, sizeY, modelId, 1));
        }

        buildingReady = true;
    }
    #endregion

    #region resource
    public void FetchResourceList()
    {
        WWW reqResource = new WWW(Web.resourceFetchList);

        StartCoroutine(WaitForResourceReq(reqResource));
    }
    private IEnumerator WaitForResourceReq(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "FetchResourceList::Empty Response");
            yield break;
        }

        //JSON data
        Debug.Log(www.text);
        JsonData res = JsonMapper.ToObject(new JsonReader(www.text));

        resources = new List<Resource>();
        for (int i = 0; i < res.Count; i++)
        {
            int id = int.Parse((string)res[i]["id"]);
            string name = (string)res[i]["name"];
            resources.Add(new Resource(id, name));
        }

        resourceReady = true;
    }
    public static ResourceType GetResourceType(int id)
    {
        return resources.Find(x => x.resourceId == id).type;
    }
    #endregion

    #region jobs
    private void FetchJobList()
    {
        WWW reqJob = new WWW(Web.getJobList);

        StartCoroutine(WaitForJobReq(reqJob));
    }
    private IEnumerator WaitForJobReq(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "FetchJobList::Empty Response");
            yield break;
        }

        //JSON data
        Debug.Log(www.text);
        JsonData jbs = JsonMapper.ToObject(new JsonReader(www.text));

        jobs = new List<Job>();
        for (int i = 0; i < jbs.Count; i++)
        {
            int id = int.Parse((string)jbs[i]["id"]);
            string name = (string)jbs[i]["name"];
            string type = (string)jbs[i]["type"];
            string callerTable = (string)jbs[i]["caller_table"];
            string targetTable = (string)jbs[i]["target_table"];
            int callerDataId = int.Parse((string)jbs[i]["caller_data_id"]);
            int rewardId = int.Parse((string)jbs[i]["reward_id"]);;
            jobs.Add(new Job
                (id,
                name,
                type,
                callerDataId,
                callerTable,
                targetTable,
                rewardId));
        }

        jobReady = true;
    }
    public static Job GetBuildJob(int dataId)
    {
        Job j = jobs.Find(x => x.callerDataId == dataId && x.type == JobType.build);
        if (j == null)
        {
            Log.WriteLine(LogType.Error, "Unable to find job");
        }

        return j;
    }
    public static Job GetUpgradeJob(int dataId)
    {
        Job j = jobs.Find(x => x.callerDataId == dataId && x.type == JobType.upgrade);
        if (j == null)
        {
            Log.WriteLine(LogType.Error, "Unable to find job");
        }

        return j;
    }
    public static Job GetGatherJob(int dataId)
    {
        Job j = jobs.Find(x => x.callerDataId == dataId && x.type == JobType.gather);
        if (j == null)
        {
            Log.WriteLine(LogType.Error, "Unable to find job");
        }

        return j;
    }
    public static JobType GetJobTypeByName(string name)
    {
        switch(name)
        {
            case "build":
                return JobType.build;
            case "gather":
                return JobType.gather;
            case "upgrade":
                return JobType.upgrade;
            default:
                Log.WriteLine(LogType.Error, "Unable to get job type");
                return JobType.build;
        }
    }
    #endregion

    #region rewards
    private void FetchRewardList()
    {
        WWW reqReward = new WWW(Web.getRewardList);

        StartCoroutine(WaitForRewardReq(reqReward));
    }
    private IEnumerator WaitForRewardReq(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "FetchRewardList::Empty Response");
            yield break;
        }

        //JSON data
        Debug.Log(www.text);
        JsonData rwrds = JsonMapper.ToObject(new JsonReader(www.text));

        rewards = new List<Reward>();
        for (int i = 0; i < rwrds.Count; i++)
        {
            int id = int.Parse((string)rwrds[i]["id"]);
            string targetTable = (string)rwrds[i]["target_table"];
            int targetId = int.Parse((string)rwrds[i]["target_id"]);
            string param = (string)rwrds[i]["param"];
            rewards.Add(new Reward(id, targetTable, targetId, param));
        }

        rewardReady = true;
    }
    public static Reward GetReward(int id)
    {
        var r = rewards.Find(x => x.rewardId == id);
        if (r == null)
        {
            Log.WriteLine(LogType.Error, "Unable to find reward id " + id);
            return null;
        }
        return r;
    }
    #endregion

    #region cost
    private void FetchCostList()
    {
        WWW reqCost = new WWW(Web.getCostList);

        StartCoroutine(WaitForCostReq(reqCost));
    }
    private IEnumerator WaitForCostReq(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "FetchCostList::Empty Response");
            yield break;
        }

        //JSON data
        Debug.Log(www.text);
        JsonData csts = JsonMapper.ToObject(new JsonReader(www.text));

        costs = new List<Cost>();
        for (int i = 0; i < csts.Count; i++)
        {
            int id = int.Parse((string)csts[i]["id"]);
            int jobId = int.Parse((string)csts[i]["job_id"]);
            int resourceId = int.Parse((string)csts[i]["resource_id"]);
            int resourceAmn = int.Parse((string)csts[i]["amount"]);
            int duration = int.Parse((string)csts[i]["duration"]);
            Cost c = new Cost(id, jobId, resourceId, resourceAmn, duration);

            Job j = jobs.Find(x => x.dataId == jobId);
            if (j != null)
                j.cost.Add(c);

            costs.Add(c);
        }

        rewardReady = true;
    }
    public static Cost GetCost(int id)
    {
        var r = costs.Find(x => x.costId == id);
        if (r == null)
        {
            Log.WriteLine(LogType.Error, "Unable to find cost id " + id);
            return null;
        }

        return r;
    }
    public static List<Cost> GetCostForJob(int jobId)
    {
        return costs.FindAll(x => x.jobId == jobId);
    }
    #endregion

    #region
    public static TargetTable GetTargetTable(string name)
    {
        switch (name)
        {
            case "resource":
                return TargetTable.Resource;
            case "building":
                return TargetTable.Building;
            case "cost":
                return TargetTable.Cost;
            case "reward":
                return TargetTable.Reward;
            case "job":
                return TargetTable.Cost;
            default:
                Log.WriteLine(LogType.Error, "Unable to find table type");
                return TargetTable.Building;
        }
    }
    #endregion
}
