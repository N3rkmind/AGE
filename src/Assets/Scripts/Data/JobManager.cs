﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class JobManager : MonoSingleton<JobManager>
{
    public List<Job> jobs;

    public bool jobReady = false;
    
    #region Fetch
    public void GetMyJobs()
    {
        WWWForm formJob = new WWWForm();
        formJob.AddField("key", User.ThisUser.Key);
        WWW reqJob = new WWW(Web.getActiveJobs, formJob);
        StartCoroutine(WaitForJobReq(reqJob));
    }
    private IEnumerator WaitForJobReq(WWW www)
    {
        yield return www;
        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Empty Response");
            yield break;
        }

        switch (www.text[0])
        {
            case '0':
                string[] data = www.text.Split('%');
                jobs = new List<Job>();

                for (int i = 1; i < data.Length; i++)
                {
                    if (data[i] == "")
                        break;

                    string[] r = data[i].Split('|');
                    int activeId = int.Parse(r[0]);
                    int dataId = int.Parse(r[1]);
                    string jobName = r[2];
                    string type = r[3];
                    int jobCallerDataId = int.Parse(r[4]);
                    string jobCallerName = r[5];
                    int jobCallerId = int.Parse(r[6]);
                    string jobTargetName = r[7];
                    int jobTargetId = int.Parse(r[8]);
                    int rewardId = int.Parse(r[9]);
                    int startTime = int.Parse(r[10]);
                    jobs.Add(new Job
                        (activeId:activeId,
                        dataId: dataId,
                        name:jobName,
                        type:type,
                        callerDataId:jobCallerDataId,
                        callerName:jobCallerName,
                        callerId:jobCallerId,
                        targetName:jobTargetName,
                        targetId:jobTargetId,
                        rewardId:rewardId,
                        startTime:startTime));
                }
                jobReady = true;
                break;
            default:
                Log.WriteLine(LogType.Information, www.text);
                //    Log.WriteLine(LogType.Error, www.text);
                break;
        }
    }
    #endregion

    #region StartJob
    public void StartBuildJob(int dataId,int activeId,int jobId)
    {
        Job j = Data.GetBuildJob(dataId);

        if (!ResourceManager.instance.ChargeResource(j.cost[0]))
        {
            Log.WriteLine(LogType.Error, "Not enough resources");
            return;
        }

        jobs.Add(new Job(j, jobId, activeId, activeId, Helper.GetTimeStamp()));
    }
    public void StartUpgradeJob(Job upgrade, int activeId, int jobId)
    {
        if (!ResourceManager.instance.ChargeResource(upgrade.cost[0]))
        {
            Log.WriteLine(LogType.Error, "Not enough resources");
            return;
        }

        jobs.Add(new Job(upgrade, jobId, activeId, activeId, Helper.GetTimeStamp()));
    }
    public void StartGatherJob(Job gather,int jobId,int activeId)
    {
        jobs.Add(new Job(gather, jobId, activeId, activeId, Helper.GetTimeStamp()));
    }
    #endregion

    #region CompleteJob
    public void CompleteJob(Job j)
    {
        // Double check
        if (j==null || !IsJobCompleted(j))
            return;

        RewardManager.instance.GrantReward(j);
    }
    public void RemoveJob(Job j)
    {
        jobs.Remove(j);
    }
    #endregion

    #region Get
    public Job GetJob(Building b)
    {
        return jobs.Find(x => x.callerId == b.ActiveId && x.callerTable == TargetTable.Building);
    }
    public static bool IsJobCompleted(Job j)
    {
        int current = Helper.GetTimeStamp();

        return current >= (j.startTime + j.cost[0].duration);
    }
    #endregion
}
