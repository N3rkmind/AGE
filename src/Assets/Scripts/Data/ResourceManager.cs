﻿using UnityEngine;
using System.Collections;

public class ResourceManager : MonoSingleton<ResourceManager>
{
    private static int wood;
    private static int food;
    private static int stone;
    private static int gold;

    public bool myResourcesReady = false;

    #region
    public void GetMyResource()
    {
        WWWForm formResource = new WWWForm();
        formResource.AddField("userId", User.ThisUser.UserId);
        WWW reqResource = new WWW(Web.resourceGet, formResource);
        StartCoroutine(WaitForResourceReq(reqResource));
    }
    private IEnumerator WaitForResourceReq(WWW www)
    {
        yield return www;
        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Empty resources response");
            yield break;
        }

        switch (www.text[0])
        {
            case '0':
                string[] data = www.text.Split('|');
                for (int i = 1; i < data.Length; i++)
                {
                    string[] r = data[i].Split('%');
                    ResourceType t = Data.GetResourceType(int.Parse(r[0]));
                    switch (t)
                    {
                        case ResourceType.Food:
                            food = int.Parse(r[1]);
                            break;
                        case ResourceType.Gold:
                            gold = int.Parse(r[1]);
                            break;
                        case ResourceType.Stone:
                            stone = int.Parse(r[1]);
                            break;
                        case ResourceType.Wood:
                            wood = int.Parse(r[1]);
                            break;
                        default:
                            Log.WriteLine(LogType.Error, "Cannot find Ressource Type");
                            break;
                    }
                }
                myResourcesReady = true;
                break;
            default:
                Log.WriteLine(LogType.Information, www.text);
                Log.WriteLine(LogType.Error, "Wrong server response");
                break;
        }
    }
    #endregion

    #region transaction
    public bool ChargeResource(Cost cost)
    {
        switch (cost.resourceType)
        {
            case ResourceType.Wood:
                if (wood >= cost.resourceAmn)
                    wood -= cost.resourceAmn;
                else
                    return false;
                break;
            case ResourceType.Stone:
                if (stone >= cost.resourceAmn)
                    stone -= cost.resourceAmn;
                else
                    return false;
                break;
            case ResourceType.Gold:
                if (gold >= cost.resourceAmn)
                    gold -= cost.resourceAmn;
                else
                    return false;
                break;
            case ResourceType.Food:
                if(food >= cost.resourceAmn)
                    food -= cost.resourceAmn;
                else
                    return false;
                break;
            default:
                Log.WriteLine(LogType.Error, "Unable to find resource type");
                break;
        }

        if (GameScene.instance != null)
            GameScene.instance.UpdateResourceText();

        return true;
    }
    public void GrantResource(Reward reward)
    {
        switch (Data.GetResourceType(reward.targetId))
        {
            case ResourceType.Wood:
                    wood += int.Parse(reward.param);
                break;
            case ResourceType.Stone:
                    stone += int.Parse(reward.param);
                break;
            case ResourceType.Gold:
                    gold += int.Parse(reward.param);
                break;
            case ResourceType.Food:
                    food += int.Parse(reward.param);
                break;
            default:
                Log.WriteLine(LogType.Error, "Unable to find resource type");
                break;
        }

        if (GameScene.instance != null)
            GameScene.instance.UpdateResourceText();
    }
    #endregion

    #region
    public static int Wood { get { return wood; } }
    public static int Food { get { return food; } }
    public static int Stone { get{ return stone; } }
    public static int Gold { get { return gold; } }
    #endregion
}