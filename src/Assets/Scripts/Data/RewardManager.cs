﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardManager : MonoSingleton<RewardManager>
{
    public void GrantReward(Job j)
    {
        switch (j.type)
        {
            case JobType.build:
                GrantBuild(j);
                break;
            case JobType.gather:
                GrantGather(j);
                break;
            case JobType.upgrade:
                GrantUpgrade(j);
                break;
            default:
                Log.WriteLine(LogType.Error, "Unable to find job type : " + j.type.ToString());
                break;
        }
    }


    private void GrantBuild(Job j)
    {
        WWWForm grantBuildForm = new WWWForm();
        grantBuildForm.AddField("key", User.ThisUser.Key);
        grantBuildForm.AddField("jobId", j.activeId);

        WWW grantBuildReq = new WWW(Web.completeBuild,grantBuildForm);

        StartCoroutine(WaitForGrantBuildReq(grantBuildReq,j));
    }
    private IEnumerator WaitForGrantBuildReq(WWW www,Job j)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Empty response");
            yield break;
        }

        Log.WriteLine(LogType.Information, www.text);

        switch (www.text[0])
        {
            case '0':
                // Replace the building
                Log.WriteLine(LogType.Good, "updated job");
                JobManager.instance.RemoveJob(j);
                Building b = Grid.instance.GetBuildingById(j.callerId);
                b.RefreshVisual();

                // Start a automatic job?
                Job gather = Data.GetGatherJob(b.DataId);
                if (gather != null)
                {
                    string[] data = www.text.Split('|');
                    JobManager.instance.StartGatherJob(gather,int.Parse(data[1]), int.Parse(data[2]));
                }

                break;

            default:
                Log.WriteLine(LogType.Error, "Unable to complete job");
                break;
        }
    }

    private void GrantGather(Job j)
    {
        WWWForm grantGatherJob = new WWWForm();
        grantGatherJob.AddField("key", User.ThisUser.Key);
        grantGatherJob.AddField("jobId", j.activeId);

        WWW grantBuildReq = new WWW(Web.completeGather, grantGatherJob);

        StartCoroutine(WaitForGrantGatherReq(grantBuildReq, j));
    }
    private IEnumerator WaitForGrantGatherReq(WWW www, Job j)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Empty response");
            yield break;
        }

        switch (www.text[0])
        {
            case '0':
                Building b = Grid.instance.GetBuildingById(j.callerId);

                // Grant actual resources
                ResourceManager.instance.GrantResource(j.reward);

                JobManager.instance.RemoveJob(j);
                // Start a automatic job?
                Job gather = Data.GetGatherJob(b.DataId);

                if (gather != null)
                {
                    string[] data = www.text.Split('|');
                    JobManager.instance.StartGatherJob(gather, int.Parse(data[1]), int.Parse(data[2]));
                }

                break;

            default:
                Log.WriteLine(LogType.Error, "Unable to complete job");
                break;
        }
    }

    private void GrantUpgrade(Job j)
    {
        WWWForm grantUpgradeJob = new WWWForm();
        grantUpgradeJob.AddField("key", User.ThisUser.Key);
        grantUpgradeJob.AddField("jobId", j.activeId);

        WWW grantUpgradeReq = new WWW(Web.completeUpgrade, grantUpgradeJob);

        StartCoroutine(WaitForGrantUpgradeReq(grantUpgradeReq, j));
    }
    private IEnumerator WaitForGrantUpgradeReq(WWW www, Job j)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "Empty response");
            yield break;
        }

        Log.WriteLine(LogType.Information, www.text);

        switch (www.text[0])
        {
            case '0':
                string[] data = www.text.Split('|');

                // Replace the building
                JobManager.instance.RemoveJob(j);
                Building b = Grid.instance.GetBuildingById(j.callerId);
                Grid.instance.UpdateBuildingAfterUpgrade(b.ActiveId, int.Parse(data[2]));

                // Start a automatic job?
                Job gather = Data.GetGatherJob(int.Parse(data[2]));
                if (gather != null)
                {
                    JobManager.instance.StartGatherJob(gather, int.Parse(data[1]), j.callerId);
                }

                break;

            default:
                Log.WriteLine(LogType.Error, "Unable to complete job");
                break;
        }
    }
}