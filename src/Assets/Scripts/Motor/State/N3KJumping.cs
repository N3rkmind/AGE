﻿using UnityEngine;
using System.Collections;

public class N3KJumping : BaseState
{
	public override void Construct ()
	{
		base.Construct ();
		motor.VerticalVelocity = motor.JumpForce;
	//	motor.Animator.SetBool ("Jump", true);
		immuneTime = 0.1f;
	}

	public override void Destruct ()
	{
		base.Destruct ();
	//	motor.Animator.SetBool ("Jump", false);
	}

	public override Vector3 ProcessMotion (Vector3 input)
	{
		MotorHelper.KillVector (ref input, motor.WallVector);
		MotorHelper.ApplySpeed (ref input, motor.Speed);
		MotorHelper.ApplyGravity (ref input, ref motor.VerticalVelocity, motor.Gravity, motor.TerminalVelocity);

		return input;
	}

	public override void Transition ()
	{
		base.Transition ();

		if (motor.VerticalVelocity < 0)
			motor.ChangeState ("N3KFalling");
	}
}