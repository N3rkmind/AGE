﻿using UnityEngine;
using System.Collections;

public class N3KWalking : BaseState
{
	public override void Construct ()
	{
		base.Construct ();
		motor.VerticalVelocity = 0.0f;
	}

	public override Vector3 ProcessMotion (Vector3 input)
	{
		MotorHelper.KillVector (ref input, motor.WallVector);
		MotorHelper.FollowVector (ref input,motor.SlopeNormal);
		MotorHelper.ApplySpeed (ref input, motor.Speed);

		return input;
	}

	public override void Transition ()
	{
		base.Transition ();

		if (!motor.Grounded ())
			motor.ChangeState ("N3KFalling");

		if (InputManager.AButton ())
			motor.ChangeState ("N3KJumping");
	}
}