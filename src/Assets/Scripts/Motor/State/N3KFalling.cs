﻿using UnityEngine;
using System.Collections;

public class N3KFalling : BaseState
{
	public bool lockMovementWhileAirborne = true;
	private Vector3 lockDirection;

	public override void Construct ()
	{
		base.Construct ();
		if (lockMovementWhileAirborne)
			LockMovementDirection ();

	//	motor.Animator.SetBool ("Jump", true);
	}

	public override void Destruct ()
	{
	//	motor.Animator.SetBool ("Jump", false);
	}

	public override Vector3 ProcessMotion (Vector3 input)
	{
		MotorHelper.KillVector (ref input, motor.WallVector);
		MotorHelper.ApplySpeed (ref input, motor.Speed);
		MotorHelper.ApplyGravity (ref input,ref motor.VerticalVelocity,motor.Gravity,motor.TerminalVelocity);

		return input + Vector3.down;
	}

	public override void Transition ()
	{
		base.Transition ();

		if (motor.Grounded ())
			motor.ChangeState ("N3KWalking");

	//	if (motor.SlopeNormal.y < motor.SlopeTreshold) 
	//		Debug.Log("Sliding State");
	}

	#region state specific

	private void LockMovementDirection()
	{
		lockDirection = new Vector3 (motor.MoveVector.x, 0, motor.MoveVector.z).normalized;
	}

	#endregion
}