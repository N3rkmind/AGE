﻿using UnityEngine;
using System.Collections;

public class BaseMotor : MonoBehaviour
{
	private const float DISTANCE_GROUNDED = 0.5f;
	private const float INNER_OFFSET_GROUNDED = 0.05f;
	private const float SLOPE_TRESHOLD = 0.55f;

	// Inspector fields
	public bool initializeOnStart = false;
	public float baseSpeed = 7.5f;
	public float baseGravity = 25.0f;
	public float baseJumpForce = 9.5f;
	public float terminalVelocity = 15.0f;
	public float slopeTreshold = 55.0f;

	protected CharacterController controller;
	protected BaseState state;
	protected Transform thisTransform;
	protected float speedModifier;
	protected float gravityModifier;
	protected float jumpForceModifier;

	// Properties
	public Vector3 MoveVector{set;get;}
	public Quaternion RotationQuaternion{set;get;}
	public Vector3 InputVector{ set; get;}
	[HideInInspector] // Not a property so we can use in ref / out
	public float VerticalVelocity;
	public Vector3 WallVector{ set; get;}
	public Vector3 SlopeNormal{set;get;}
	public Animator Animator{ set; get;}
	public CollisionFlags ColFlags{ set; get;}

	// Moving Platform Support
	private Transform activePlatform;
	private Vector3 activeLocalPoint;
	private Vector3 activeGlobalPoint;
	private Vector3 lastPlatformVelocity;

	#region Start
	private void Start()
	{
		if (initializeOnStart)
			Init ();
	}
	public virtual void Init()
	{
		controller = GetComponent<CharacterController> ();
		Animator = GetComponent<Animator> ();
		thisTransform = transform;

		if (!controller)
			Debug.Log ("Add a CharacterController to your obj");
		
		state = GetComponent<N3KWalking> ();
		state.Construct ();
	}
	#endregion

	#region Update
	private void Update()
	{
		UpdateMotor ();
	}
	protected virtual void UpdateMotor()
	{
		// Ask Mobility state to Calculate Motion
		MoveVector = state.ProcessMotion(MoveVector);
		RotationQuaternion = state.ProcessRotation(MoveVector);

		// Check for StateTransitions
		state.Transition();

		// Moving Platform Pre-move
		MovingPlatformPreMove();

		// Move the Controller
		Move();
		Rotate();

		// Moving Platform Post-move
		MovingPlatformPostMove();
	}
	protected virtual void Move()
	{
		ColFlags = controller.Move (MoveVector * Time.deltaTime);
		WallVector = (((ColFlags & CollisionFlags.Sides) != 0) ? WallVector : Vector3.zero);
	}
	protected virtual void Rotate()
	{
        if(MoveVector.magnitude != 0)
		    thisTransform.rotation = RotationQuaternion;
	}
	#endregion

	#region Getters
	public float Speed{ get { return baseSpeed + speedModifier;} }
	public float Gravity{ get { return baseGravity + gravityModifier;} }
	public float JumpForce{ get { return baseJumpForce + jumpForceModifier;} }
	public float SlopeTreshold{get{ return slopeTreshold;}}
	public float TerminalVelocity{get{ return terminalVelocity;}}
	#endregion

	#region Methods
	public virtual bool Grounded()
	{
		float yRay = controller.bounds.center.y - (controller.height * 0.5f) + 0.3f;
		RaycastHit hit;

		// Mid
		if(Physics.Raycast(new Vector3(controller.bounds.center.x,yRay,controller.bounds.center.z),-Vector3.up,out hit,DISTANCE_GROUNDED))
		{
			SlopeNormal = hit.normal;
			return (SlopeNormal.y > SLOPE_TRESHOLD)?true:false;
		}

		// Front-Right
		if(Physics.Raycast(new Vector3(controller.bounds.center.x + (controller.bounds.extents.x - INNER_OFFSET_GROUNDED),yRay,controller.bounds.center.z + (controller.bounds.extents.z - INNER_OFFSET_GROUNDED)),-Vector3.up,out hit,DISTANCE_GROUNDED))
		{
			SlopeNormal = hit.normal;
			return (SlopeNormal.y > SLOPE_TRESHOLD)?true:false;
		}

		// Front-Left
		if(Physics.Raycast(new Vector3(controller.bounds.center.x - (controller.bounds.extents.x - INNER_OFFSET_GROUNDED),yRay,controller.bounds.center.z + (controller.bounds.extents.z - INNER_OFFSET_GROUNDED)),-Vector3.up,out hit,DISTANCE_GROUNDED))
		{
			SlopeNormal = hit.normal;
			return (SlopeNormal.y > SLOPE_TRESHOLD)?true:false;
		}
		// Back Right
		if(Physics.Raycast(new Vector3(controller.bounds.center.x + (controller.bounds.extents.x - INNER_OFFSET_GROUNDED),yRay,controller.bounds.center.z - (controller.bounds.extents.z - INNER_OFFSET_GROUNDED)),-Vector3.up,out hit,DISTANCE_GROUNDED))
		{
			SlopeNormal = hit.normal;
			return (SlopeNormal.y > SLOPE_TRESHOLD)?true:false;
		}
		// Back Left
		if(Physics.Raycast(new Vector3(controller.bounds.center.x - (controller.bounds.extents.x - INNER_OFFSET_GROUNDED),yRay,controller.bounds.center.z - (controller.bounds.extents.z - INNER_OFFSET_GROUNDED)),-Vector3.up,out hit,DISTANCE_GROUNDED))
		{
			SlopeNormal = hit.normal;
			return (SlopeNormal.y > SLOPE_TRESHOLD)?true:false;
		}

		return false;
	}
	protected virtual void OnControllerColliderHit(ControllerColliderHit hit)
	{
		if (hit.moveDirection.y < 0.9f && hit.normal.y > 0.5f)
			activePlatform = hit.collider.transform;

		if (VerticalVelocity > 0 && ((ColFlags & CollisionFlags.Above) != 0))
			VerticalVelocity = 0;
	}
	public virtual void ChangeState(string stateName)
	{
		state.Destruct ();
		state = GetComponent (stateName) as BaseState;
		state.Construct ();
	}
	protected void MovingPlatformPreMove()
	{
		if (activePlatform != null) 
		{
			var newGlobalPlatformPoint = activePlatform.TransformPoint (activeLocalPoint);
			var moveDistance = (newGlobalPlatformPoint - activeGlobalPoint);
			if (moveDistance != Vector3.zero)
				controller.Move (moveDistance);

			lastPlatformVelocity = moveDistance / Time.deltaTime;
		} 
		else
		{
			lastPlatformVelocity = Vector3.zero;
		}
	}
	protected void MovingPlatformPostMove()
	{
		if (activePlatform != null)
		{
			activeGlobalPoint = thisTransform.position;
			activeLocalPoint = activePlatform.InverseTransformPoint (activeGlobalPoint);
		}
	}
	#endregion
}