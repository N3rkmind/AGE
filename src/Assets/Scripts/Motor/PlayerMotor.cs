﻿using UnityEngine;
using System.Collections;

public class PlayerMotor : BaseMotor
{
	public Transform CameraTransform{ set; get;}

	public override void Init ()
	{
		base.Init ();

		// Initialize the player's Camera
		CameraTransform = Camera.main.transform;
	}

	protected override void UpdateMotor ()
	{
		// Get the Joystick Input, store them in MoveVector
		Vector3 input = PoolInput ();
		InputVector = input;

		// Rotate the MoveVector with the camera's direction
		MotorHelper.RotateWithView (ref input,CameraTransform);

		// Transfer inputs to the MoveVector before processing it
		MoveVector = input;

		// Ask Mobility state to Calculate Motion
		MoveVector = state.ProcessMotion(MoveVector);
		RotationQuaternion = state.ProcessRotation(MoveVector);
		if(Animator != null)
			state.ProcessAnimation (Animator);

		// Check for StateTransitions
		state.Transition();

		// Moving Platform Pre-move
		MovingPlatformPreMove();

		// Move the Controller
		Move();
		Rotate();

		// Moving Platform Post-move
		MovingPlatformPostMove();
	}

	private Vector3 PoolInput()
	{
		Vector3 dir = Vector3.zero;

		dir.x = InputManager.MainHorizontal ();
		dir.z = InputManager.MainVertical ();

		if (dir.sqrMagnitude > 1)
			dir.Normalize ();

		return dir;
	}
}
