﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AuthScene : MonoBehaviour
{
    public CanvasGroup canvasGroup;

    private void Awake()
    {
        if (FindObjectOfType<Authentication>() == null)
        {
            Log.WriteLine(LogType.Information, "Redirecting to auth");
            SceneManager.LoadScene("PreLoader");
            return;
        }

        canvasGroup.alpha = 0;
    }

    private void Update()
    {
        if (Time.timeSinceLevelLoad > 2.2f)
        {
            if(canvasGroup.alpha != 1)
            {
                canvasGroup.alpha = Time.timeSinceLevelLoad - 2.2f;
            }
        }
    }

    public void OnLoginButton()
    {
        string email = GameObject.Find("EmailInput").GetComponent<InputField>().text;
        string password = GameObject.Find("PasswordInput").GetComponent<InputField>().text;

        if (email == "")
        {
            Log.WriteLine(LogType.Error, "Must enter a email");
            return;
        }

        if (password == "")
        {
            Log.WriteLine(LogType.Error, "Must enter a password");
            return;
        }

        Authentication.instance.LoginRequest(email, password);
    }

    public void OnGoogleButton()
    {
        Application.ExternalCall("OnGoogleButton");
    }

    public void OnCreateButton()
    {
        SceneManager.LoadScene("CreateUser");
    }

    public void OnGymButton()
    {
        SceneManager.LoadScene("Gym");
    }
}
