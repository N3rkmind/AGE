﻿using UnityEngine;

public class Preloader : MonoBehaviour
{
    private void Start()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Auth", UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }
}
