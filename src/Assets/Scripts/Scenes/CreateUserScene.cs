﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class CreateUserScene : MonoBehaviour
{
    public CanvasGroup firstFormGroup;
    public CanvasGroup secondFormGroup;

    private string email;
    private string password;

    private WWW firstRequest;
    private WWWForm firstForm;
    private WWW secondRequest;
    private WWWForm secondForm;

    private void Start()
    {
        if (User.ThisUser != null && User.ThisUser.Username != "")
        {
            // We came from the google sign in
            MoveToSecondForm();
            email = User.ThisUser.name;
            password = "0";
        }
    }

    #region btn
    public void OnCreateButton()
    {
        string email = GameObject.Find("EmailInput").GetComponent<InputField>().text;
        string password = GameObject.Find("PasswordInput").GetComponent<InputField>().text;
        string confirm = GameObject.Find("ConfirmInput").GetComponent<InputField>().text;

        SendFirstRequest(email, password, confirm);
    }
    public void SendFirstRequest(string email)
    {
        // For google auth
    }
    public void SendFirstRequest(string email, string password, string confirm)
    {
        if (email == "")
        {
            Log.WriteLine(LogType.Error, "Must enter a email");
            return;
        }

        if (password == "")
        {
            Log.WriteLine(LogType.Error, "Must enter a password");
            return;
        }

        if (password != confirm)
        {
            Log.WriteLine(LogType.Error, "Passwords don't match");
            return;
        }

        this.email = email;
        this.password = password;

        firstForm = new WWWForm();
        firstForm.AddField("email", email);

        firstRequest = new WWW(Web.createCheckEmail, firstForm);
        StartCoroutine(WaitForFirstRequest(firstRequest));
    }
    private IEnumerator WaitForFirstRequest(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "CreateUserScene::WaitForFirstRequest::No response from server");
            yield break;
        }

        switch (www.text[0])
        {
            case '0':
                Log.WriteLine(LogType.Good, "Email is available!");
                MoveToSecondForm();
                break;
            case '2':
                Log.WriteLine(LogType.Error, "Email is taken!");
                break;
            default:
                Log.WriteLine(LogType.Error, "CreateUserScene::WaitForFirstRequest::Response : " + www.text);
                break;
        }
    }
    private void MoveToSecondForm()
    {
        firstFormGroup.alpha = 0;
        firstFormGroup.blocksRaycasts = false;
        firstFormGroup.interactable = false;

        secondFormGroup.alpha = 1;
        secondFormGroup.blocksRaycasts = true;
        secondFormGroup.interactable = true;
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene("Auth");
    }
    #endregion

    #region Second Screen
    
    public void OnSecondCreateButton()
    {
        string nick = GameObject.Find("NicknameInput").GetComponent<InputField>().text;
        SendSecondRequest(nick);
    }
    public void MoveToFirstForm()
    {
        firstFormGroup.alpha = 1;
        firstFormGroup.blocksRaycasts = true;
        firstFormGroup.interactable = true;

        secondFormGroup.alpha = 0;
        secondFormGroup.interactable = false;
        secondFormGroup.blocksRaycasts = false;
    }
    public void SendSecondRequest(string displayName)
    {
        if (displayName == "")
        {
            Log.WriteLine(LogType.Error, "Must enter a nickname");
            return;
        }

        secondForm = new WWWForm();
        secondForm.AddField("email", email);
        secondForm.AddField("displayName", displayName);

        // Hash password
        Dictionary<string, string> headers = secondForm.headers;
        byte[] rawData = secondForm.data;
        headers["Auth"] = Helper.Sha256String(password);

        secondRequest = new WWW(Web.createUser,rawData, headers);
        StartCoroutine(WaitForSecondRequest(secondRequest));
    }
    private IEnumerator WaitForSecondRequest(WWW www)
    {
        yield return www;

        if (www.text == "")
        {
            Log.WriteLine(LogType.Error, "CreateUserScene::WaitForSecondRequest::No response from server");
            yield break;
        }

        switch (www.text[0])
        {
            case '0':
                Log.WriteLine(LogType.Good, "Account has been created!");
                CreationSuccess();
                break;
            default:
                Log.WriteLine(LogType.Information, www.text);
                Log.WriteLine(LogType.Error, "CreateUserScene::WaitForSecondRequest::Response : " + www.text);
                break;
        }
    }

    public void CreationSuccess()
    {
        SceneManager.LoadScene("Auth");
    }
    #endregion
}
