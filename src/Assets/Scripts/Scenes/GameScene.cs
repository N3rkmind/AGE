﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameScene : MonoSingleton<GameScene>
{
    public ActionBar ActionBar { set; get; }
    public Grid Grid { set; get; }

    public GameObject navigationMenu;
    public Transform buildMenu;

    private Text foodText;
    private Text stoneText;
    private Text woodText;
    private Text goldText;

    private void Start()
    {
        if (FindObjectOfType<Authentication>() == null)
        {
            Log.WriteLine(LogType.Information, "Redirecting to auth");
            SceneManager.LoadScene("PreLoader");
            return;
        }
        
        ActionBar = FindObjectOfType<ActionBar>();
        Grid = FindObjectOfType<Grid>();

        foodText = GameObject.Find("FoodAmn").GetComponent<Text>();
        stoneText = GameObject.Find("StoneAmn").GetComponent<Text>();
        woodText = GameObject.Find("WoodAmn").GetComponent<Text>();
        goldText = GameObject.Find("GoldAmn").GetComponent<Text>();

        UpdateResourceText();

        FillBuildMenu();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            NavOpenNavigation();
            Grid.ResetGrid();
        }
    }

    #region build
    public void FillBuildMenu()
    {
        foreach (Building b in Data.buildings)
        {
            var go = Instantiate(Resources.Load("UI/Building") as GameObject);
            go.transform.SetParent(buildMenu);

            Button btn = go.GetComponent<Button>();
            int m = b.DataId;
            btn.onClick.AddListener(() => PreviewBuilding(m));
            btn.onClick.AddListener(() => ActionBar.CloseMenu());
        }
    }
    public void PreviewBuilding(int index)
    {
        Grid.PreviewBuilding(index);
    }
    #endregion

    #region navigation
    public void NavOpenNavigation()
    {
        navigationMenu.SetActive(!navigationMenu.activeSelf);
    }
    public void NavBackToMainMenu()
    {
        Log.WriteLine(LogType.Information, "Going back to main menu");
        SceneManager.LoadScene("Menu");
    }
    public void NavOption()
    {
        Log.WriteLine(LogType.Normal, "Opening Options");
    }
    public void NavCancel()
    {
        NavOpenNavigation();
        Log.WriteLine(LogType.Normal, "Canceling Navigation");
    }
    #endregion

    #region UI
    public void UpdateResourceText()
    {
        foodText.text = ResourceManager.Food.ToString();
        stoneText.text = ResourceManager.Stone.ToString();
        woodText.text = ResourceManager.Wood.ToString();
        goldText.text = ResourceManager.Gold.ToString();
    }
    #endregion
}