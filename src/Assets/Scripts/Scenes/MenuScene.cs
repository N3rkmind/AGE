﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScene : MonoSingleton<MenuScene>
{
    public Button gridButton;

    private bool gridReady = false;
    private float checkInterval = 0.5f;
    private float lastCheck;

    public override void Init()
    {
        gridButton.interactable = false;

        if (FindObjectOfType<Authentication>() == null)
        {
            Log.WriteLine(LogType.Information, "Redirecting to auth");
            UnityEngine.SceneManagement.SceneManager.LoadScene("PreLoader");
            return;
        }

        GameObject.Find("NameLabel").GetComponent<Text>().text = string.Format("Welcome back <color=green>{0}</color>", User.ThisUser.Username);

        // Launch the fetching jobs
        ResourceManager.instance.GetMyResource();
        JobManager.instance.GetMyJobs();
    }

    private void Update()
    {
        if (!gridReady)
        {
            if (Time.time - lastCheck > checkInterval)
            {
                lastCheck = Time.time;
                // RESOURCE && JOBS && GUILD && TROOPS $$
                if (ResourceManager.instance.myResourcesReady && JobManager.instance.jobReady)
                {
                    // Set Resources
                    GameObject.Find("FoodAmn").GetComponent<Text>().text = ResourceManager.Food.ToString();
                    GameObject.Find("WoodAmn").GetComponent<Text>().text = ResourceManager.Wood.ToString();
                    GameObject.Find("StoneAmn").GetComponent<Text>().text = ResourceManager.Stone.ToString();
                    GameObject.Find("GoldAmn").GetComponent<Text>().text = ResourceManager.Gold.ToString();

                    SetGridReady();
                }
            }
        }
    }

    public void SetGridReady()
    {
        gridButton.interactable = true;
    }

    #region button actions
    public void ToGrid()
    {
        SceneManager.LoadScene("Game");
    }
    public void LogoutButton()
    {
        Authentication.instance.Disconnect();
    }
    #endregion
}
