﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Security.Cryptography;
using System;

public static class Helper
{
    public static string Sha256String(string input)
    {
        StringBuilder Sb = new StringBuilder();

        using (SHA256 hash = SHA256Managed.Create())
        {
            Encoding enc = Encoding.UTF8;
            byte[] result = hash.ComputeHash(enc.GetBytes(input));

            foreach (byte b in result)
                Sb.Append(b.ToString("x2"));
        }

        return Sb.ToString();
    }

    public static Int32 GetTimeStamp()
    {
        Int32 unixTimeStamp;
        DateTime currentTime = DateTime.Now;
        DateTime zuluTime = currentTime.ToUniversalTime();
        DateTime unixEpoch = new DateTime(1970, 1, 1);
        unixTimeStamp = (Int32)(zuluTime.Subtract(unixEpoch)).TotalSeconds;

        return unixTimeStamp;
    }

    public static string RemoveSpecialCharacters(string str)
    {
        StringBuilder sb = new StringBuilder();
        foreach (char c in str)
        {
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }

    public static GameObject CreateFloorQuad()
    {
        GameObject go = new GameObject("Quad");
        MeshFilter mf = go.AddComponent<MeshFilter>();
        MeshRenderer mr = go.AddComponent<MeshRenderer>();

        Mesh m = new Mesh();
        m.vertices = new Vector3[]
        {
            new Vector3(0,0,0),
            new Vector3(1,0,0),
            new Vector3(1,0,1),
            new Vector3(0,0,1)
        };

        m.uv = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(0,1),
            new Vector2(1,1),
            new Vector2(1,0)
        };

        m.triangles = new int[] { 0, 2, 1, 0, 3, 2 };

        mf.mesh = m;
        m.RecalculateBounds();
        m.RecalculateNormals();

        return go;
    }
}
